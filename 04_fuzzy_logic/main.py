import json
from youtubesearchpython import VideosSearch   
        
MAX_SEARCH_RESULTS = 10


def search_youtube():
    """
    Asks a user for input, searches youtube for this keyword and stores 
    the results in a JSON file.
    """
    req = input("Enter youtube search keyword: ")
    videosSearch = VideosSearch(req, limit = MAX_SEARCH_RESULTS)
    if videosSearch.result().get('result'):
        with open("results.json", "w") as file:
            rlist = {
                "keyword": req,
                "results": []
            }
            for result in videosSearch.result()['result']:
                r = {
                    "id": result.get("id"),
                    "title": result.get("title"),
                    "viewCount.text": result.get("viewCount").get("text"),
                    "channel.name": result.get("channel").get("name"),
                    "link": result.get("link")
                }
                rlist.get("results").append(r)
            # So we can see what the results are.
            file.write(json.dumps(rlist))
            

def compute_longest_common_substring(s1, s2):
    dict =  {}
    # For every character in the first string compare it to 
    # all characters in the second string.
    for i in range(len(s1)):
        for j in range(len(s2)):
            # For every match we store the index for the position in 
            # the first and previous string. To check a sequence of 
            # matching characters we simply increment the previous
            # matching count (i-1 and j-1) - if available. 
            if s1[i] == s2[j]:
                current_count = 0
                if (i-1, j-1) in dict:
                    current_count = dict[(i-1, j-1)] 
                dict[(i,j)] = current_count + 1
    # The dictionary contains the characters and the count of matching 
    # sequence of characters. We sort it by the count (desc) and get
    # the first with the length of the sequence and location of the 
    # last character in the two strings (position i in the first string
    # and position j in seconds sting).
    sortdict = sorted(dict.items(), key=lambda x:x[1], reverse=True)
    return  sortdict[0] # (i, j), length


def compute_similarity(s1, s2):
    """
    Computes the similarity between two strings s1 and s2 based on
    the longest common sequence of characters.
    
    Loosely based on Gestalt Pattern Recognition.
    """

    # Find longest common substring.
    _, length = compute_longest_common_substring(s1, s2)

    # This metric will compute a ratio between the (double) length of 
    # the matching sequence and the length of both string, where 1.0
    # is a perfect match.
    metric = 2 * length / (len(s1) + len(s2))
    return metric

def ratcliff_obershelp(s1, s2):
    length = compute_longest_common_substring_recursive(s1, s2, 3)
    return 2 * length / (len(s1) + len(s2))
    

def compute_longest_common_substring_recursive(s1, s2, count):
    length = 0 
    if count == 0 or len(s1) == 0 or len(s2) == 0:
        return length

    (i,j), l = compute_longest_common_substring(s1,s2)
    length = l
    if l > 0:
        length += compute_longest_common_substring_recursive(
            s1.replace(s1[i - l + 1: i + 1], ""), 
            s2.replace(s2[j - l + 1: j + 1], ""),
            count - 1
        )

    return length


def sanitize(s):
    """
    This function tries to remove some "unused" character to get a 
    better metric.
    """ 
    return s \
        .replace(" - ", " ").replace("- ", " ").replace(" -", " ") \
        .replace(" | ", " ").replace("| ", " ").replace(" |", " ")


def similarity_from_results():
    """ 
    Reads the results from a JSON file, sanitizes the strings and
    computes the similarity between all titles and the keyword.
    """
    d = {}
    with open("results.json", "r") as file:
        contents =  json.load(file)
        s1 = sanitize(contents.get("keyword").casefold())
        for index, result in enumerate(contents.get("results")):
            s2 = sanitize(result.get("title").casefold())
            d[f"({index+1}) {s2}"] = [
                compute_similarity(s1, s2),
                ratcliff_obershelp(s1, s2)
            ]

    # Sort the result and print them.
    sortdict = sorted(d.items(), key=lambda x:x[1], reverse=True)
    print("Results are shown as follows: 'metric: (youtube ranking) title'")
    for key, value in dict(sortdict).items():
        print(f"{round(value[0], 3)}, {round(value[1], 3)}:  {key}")


if __name__ == "__main__":
    search_youtube()
    similarity_from_results()
    