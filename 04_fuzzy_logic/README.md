# Fuzzy logic

This small application attempts to compute some metric for comparing similarity between song titles. Is asks the user for some keywords to search on youtube. It returns a list of results an stores them into a JSON file (called results.json). This file is then parsed and the similarity between the keyword and song titles computed. 

To run the program type the following in a terminal:
```
poetry install && poetry run python main.py
```

## Resources
* [String similarity — the basic know your algorithms guide!](https://itnext.io/string-similarity-the-basic-know-your-algorithms-guide-3de3d7346227)