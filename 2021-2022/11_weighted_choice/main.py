import random
from select import select

NUM_PARTICIPANTS = 10
NUM_ROUNDS = 1
NUM_SONGS = 5

FACTOR = 1000

class WeightedSelection:

    def __init__(self):
        self._participants = []
    
    def add_participant(self, participant):
        self._participants.append(participant)

    def select(self):
        lil_list = []
        for participant in self._participants:
            for _ in range(participant["weight"]):
                lil_list.append(participant)
            participant["weight"] += 1
        selected = random.choice(lil_list)
        selected["weight"] = 1
        return selected


def test():
    num_first_selected = 0
    num_first_not_selected_second_time = 0
    for _ in range(100 * FACTOR):
        ws = WeightedSelection()
        participants = [ { "name": i, "weight": 1 } for i in range(NUM_PARTICIPANTS)]
        participants[0]["weight"] = 100

        for participant in participants:
            ws.add_participant(participant)

        if ws.select() == participants[0]:
            num_first_selected +=1
        if ws.select() != participants[0]:
            num_first_not_selected_second_time += 1

    print(num_first_selected)
    print(num_first_not_selected_second_time)
    print((num_first_selected - num_first_not_selected_second_time) // FACTOR)

    if num_first_selected // FACTOR > 90:
        print("Werkt")
    

# def choose_first_please(participants):
#     participants[0]["weight"] = 100
#     lijstje = []
#     for participant in participants:
#         for _ in range(participant["weight"]):
#             lijstje.append(participant)
#     return random.choice(lijstje)


# def test_old():
#     participants = [ { "name": i, "weight": 1 } for i in range(NUM_PARTICIPANTS)]
#     num_first_selected = 0
#     for _ in range(100 * FACTOR):
#         if choose_first_please(participants) == participants[0]:
#             num_first_selected +=1
#     print(num_first_selected)
#     if num_first_selected // FACTOR > 90:
#         print("Werkt")


def main():
    participants = [ { "name": i, "weight": 1 } for i in range(NUM_PARTICIPANTS)]
    print(participants)

    participants[0]["weight"] = 100
    for _ in range(NUM_ROUNDS):
        # selected = random.choice(participants)
        lijstje = []
        for participant in participants:
            for _ in range(participant["weight"]):
                lijstje.append(participant)
        selected = random.choice(lijstje)
        for participant in participants:
            participant["weight"] += 1   
        selected["weight"] = 1
        print(selected)
    print("----")
    for participant in participants:
        print(participant)


if __name__ == "__main__":
    test()