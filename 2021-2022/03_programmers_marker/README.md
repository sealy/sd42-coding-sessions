# Programmers market

Features:
 - [] Register a programmer
 - [*] Print the list of programmers
 - [] Configure the programmer's skills (programming language)
 - [] Remove a programmer
 - [] Review a programmer
 - [] Search for a programmer (based on skills, sort on rating)