OPTIONS = """1. List programmers
2. Register a programmer
3. Update programmer
4. Remove a programmer
0. Exit application
"""

def print_programmers(programmers_list):
    if len(programmers_list) == 0:
        print(" - We have no programmers :(")
    else:
        for programmer in programmers_list:
            print(f" * {programmer}")


def register_programmer(programmers_list, name):
    programmers_list.append(name)


def update_programmer():
    pass


def remove_programmer():
    pass


def main():
    programmers = []
    print("Welcome to the programmers market")
    while True:
        print(OPTIONS)
        selection = input("Please select one of the options: ")
        while selection not in ["0", "1", "2", "3", "4"]:
            selection = input("Invalid input. Please try again: ")

        selection = int(selection)
        if selection == 0:
            break
        elif selection == 1:
            print_programmers(programmers)
        elif selection == 2:
            name = input("What is the programmer's name: ")
            register_programmer(programmers, name)
        elif selection == 3:
            update_programmer()
        elif selection == 4:
            remove_programmer()

if __name__ == "__main__":
    main()
