from crate import Crate

def compute_rest_consumption(consumption, num_crates):
    while consumption >= Crate.MAX_NUM_BOTTLES and num_crates > 0:
            consumption -= Crate.MAX_NUM_BOTTLES
            num_crates -= 1
    return (consumption, num_crates)