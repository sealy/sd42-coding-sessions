class Persoon():
    
    def __init__(self, naam, leeftijd):
        self.naam = naam
        self.leeftijd = leeftijd

    def __str__(self):
        return f"Ik heet {self.naam} en ben {self.leeftijd} jaar oud"

class Crate():
    MAX_NUM_BOTTLES = 24

    def __init__(self, is_bought, num_consumptions):
        self.is_bought = is_bought 
        self.num_consumptions = num_consumptions
 
    def __str__(self):
        html = '<div class="crate">\n'
        for bottle in range(self.MAX_NUM_BOTTLES):
            filled = ''
            if bottle < self.num_consumptions:
                filled = ' filled'
            html += f'<div class="bottle{filled}"></div>\n'
        html += '</div>'
        return html

def main():
    c = Crate(True, 10)
    print(c)

if __name__ == "__main__":
    main()