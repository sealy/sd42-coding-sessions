# Bier afstreep lijst

Dit is onze awesome bier afstreep applicatie.

Features:
[X] Deelnemers kunnen toevoegen
[ ] Deelnemers verwijderen
[X] Registreren dat er een biertje gedronken is
[X] Registreren dat er een kratje gekocht is
[X] Voorraad telling 
    (sanity check -> aangeven hoeveel bier er op dit moment in de koelkast moet zitten)
[X] Implementeer opslag
[X] Implementeer lezen vanaf opslag
[X] Bepalen wie de bock is.
[ ] HTML export maken waarin de kratjes en consumptie visueel worden weergegeven
    [X] Template gemaakt voor bier kratjes en consumptie.
    - TODO: html exporter aanpassen zodat het de structuur van de template exporteert.
[ ] Handel het biertegoed of schuld af bij het verwijderen van een deelnemer.
