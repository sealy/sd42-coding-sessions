import heapq
from crate import Crate
from util import compute_rest_consumption
from random import choice, shuffle

class TooYoungException(Exception):
    pass

class InvalidNameException(Exception):
    pass


participants = []
AMOUNT_OF_BEER_IN_CRATE = 24
DEFAULT_NUMBER_OF_CRATES = 0
DEFAULT_CONSUMPTION = 0


def save_data(filename):
    with open(filename,'w') as file:
        keys = ["name","age", "number_of_crates", "consumption"]
        lines = []
        lines.append(";".join(keys)+"\n")
        for p in participants:
            values = [str(p.get(key,'')) for key in keys]
            lines.append(";".join(values)+"\n")
        file.writelines(lines)


def load_data(filename):
    with open(filename) as file:
        lines = file.readlines()
        header = None
        for line in lines:
            if header is None:
                header = line.rstrip().split(";")
            else:
                participant = {}
                item = line.strip().split(";")
                for index, key in enumerate(header):
                    participant[key] = item[index]
                    if item[index].isdigit():
                        participant[key] = int(item[index])
                participants.append(participant)
    

def list_participants():
    return participants


def add_participant(participant_name, participant_age):
    if int(participant_age) < 18:
        raise TooYoungException()
    elif len(participant_name) < 3:
        raise InvalidNameException()
    else: 
        participant = {
            "name": participant_name,
            "age": participant_age,
            "number_of_crates": DEFAULT_NUMBER_OF_CRATES,
            "consumption": DEFAULT_CONSUMPTION,
        }
        participants.append(participant)
    return participants


def remove_participant(participant_name):
    participant = find_participant(participant_name) 
    if participant == None:
        print("ERROR: 404! Neem contact op met je systeembeheerder")
        return

    if participant['consumption'] < participant['number_of_crates'] * Crate.MAX_NUM_BOTTLES:
        # Biertegoed
        beneficiary = choice(participants)
        while beneficiary == participant:
            beneficiary = choice(participants)
        print(f'WARN: Deelnemer heeft een bier TEGOED dat wordt overgedragen aan {beneficiary["name"]}!')
        consumption, num_crates = compute_rest_consumption(participant['consumption'], participant['number_of_crates'])
        beneficiary['consumption'] += consumption
        beneficiary['number_of_crates'] += num_crates
        participants.remove(participant)
    elif participant['consumption'] > participant['number_of_crates'] * Crate.MAX_NUM_BOTTLES:
        # Bierschuld
        print('WARN: Deelnemer heeft een bier SCHULD dar "eerlijk" over de andere deelnemers wordt verdeeld!')
        consumption, _ = compute_rest_consumption(participant['consumption'], participant['number_of_crates'])
        equal_parts = consumption // (len(participants) - 1)
        rest_consumption = consumption % (len(participants) - 1)
        participants.remove(participant)
        for p in participants:
            p["consumption"] += equal_parts
        shuffle(participants)
        for p in participants:
            if rest_consumption == 0:
                break
            p["consumption"] += 1
            rest_consumption -= 1
        
        # beneficiary = choice(participants)
        # beneficiary['consumption'] += rest_consumption
        
    else:
        participants.remove(participant)


def register_beer_crate(participant_name, number_of_crates):    
    participant = find_participant(participant_name)
    if participant:
        current_number_of_crates = participant.get("number_of_crates", 0) 
        participant["number_of_crates"] = current_number_of_crates + number_of_crates
        return True
    return False


def register_beer_consumption(participant_name, amount_of_beer):
    participant = find_participant(participant_name)
    stock = get_stock()
    if participant and amount_of_beer < stock:
        current_consumption = participant.get("consumption", 0) 
        participant["consumption"] = current_consumption + amount_of_beer
        return True
    return False


def find_participant(name):
    for participant in participants:
        if name == participant["name"]:
            return participant
    return None
    

def get_stock():
    stock = 0
    for participant in participants:
        stock += participant.get("number_of_crates", 0) * AMOUNT_OF_BEER_IN_CRATE
        stock -= participant.get("consumption", 0)
    return stock


def get_bock():
    heap = []
    for participant in participants:
        bock_index = participant.get("number_of_crates", 0) * AMOUNT_OF_BEER_IN_CRATE - participant.get("consumption", 0)
        heap.append((bock_index, participant))
    heapq.heapify(heap)
    return heapq.heappop(heap)[1]
    