
class Crate():
    MAX_NUM_BOTTLES = 24

    def __init__(self, is_bought, num_consumptions):
        self.is_bought = is_bought 
        self.num_consumptions = num_consumptions
 
    def html(self):
        bought = ''
        if self.is_bought:
            bought = ' crate-bought'
        html = f'<div class="crate{bought}">\n'
        for bottle in range(self.MAX_NUM_BOTTLES):
            filled = ''
            if bottle < self.num_consumptions:
                filled = ' filled'
            html += f'<div class="bottle{filled}"></div>\n'
        html += '</div>'
        return html