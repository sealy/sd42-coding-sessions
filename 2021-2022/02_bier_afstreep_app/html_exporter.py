from crate import Crate
from util import compute_rest_consumption

HTML_EXPORT_FILE = "02_bier_afstreep_app/export/index.html"
NUM_CRATES = 5


def export(participants, stock):
    list_participants = "<ol>"
    for p in participants:
        crates = ""
        consumption, num_crates= compute_rest_consumption(p['consumption'], p['number_of_crates'])
        for crate in range(NUM_CRATES):
            c = Crate(False, consumption)
            consumption -= Crate.MAX_NUM_BOTTLES
            if crate < num_crates:
                c.is_bought = True
            crates += c.html()
        
        list_participants += f"""<li>
            <div>
                <p>{p.get('name')} [Gekocht: {p['number_of_crates']} kratjes / Gedronken: {p['consumption']} flesjes] </p>
                <div class='crates'>
                    {crates}
                </div>
            </div>
        </li>"""
    list_participants += "</ol>"
    
    with open(HTML_EXPORT_FILE,'w') as file:
        file.write(f"""<!DOCTYPE html>
<html>
<head>
    <title>Wie is de bock?</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <main>
        <section>
            {list_participants}
        </section>
        <section>
            <p>
                In de koelkast ligt {stock} bier!
            </p>
        </section>    
    </main>
</body>
</html>""")