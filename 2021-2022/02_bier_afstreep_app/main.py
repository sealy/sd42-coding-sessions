from os import lseek
import registration_module as rg
import html_exporter as he

BEER_FILE = "beer_participants.csv"


def main():

    print("*********************")
    print("*  Wie is de bock?  *")
    print("*********************")

    rg.load_data(BEER_FILE)

    while True:
        print("""1. Voeg een deelnemer toe
2. Verwijder deelnemer
3. Toon deelnemers
4. Registreer een kratje
5. Registreer een consumptie
6. Selecteer een Bock
7. Export naar HTML
0. Sluit applicatie""")

        while True:
            selection = input("Kies een van de bovenstaande opties: ")
            if selection.isdigit() and 0 <= int(selection) <= 7:
                break
            else:
                print("Ongeldige keuze. Probeer het nog een keer.")
        
        selection = int(selection)
        if selection == 0:
            break
        elif selection == 1:
            participant_name = input("Voer de naam van de deelnemer in: ")
            participant_age = input("Voer de leeftijd van de deelnemer in: ")
            try:
                rg.add_participant(participant_name, participant_age)
                rg.save_data(BEER_FILE)
            except:
                print("Er is een fout opgetreden")
        elif selection == 2:
            participant_name = input("Wie mag er weg? Vul hier zijn naam in: ")
            rg.remove_participant(participant_name)
            rg.save_data(BEER_FILE)
        elif selection == 3:
            print(f"[Bier in koelkast: {rg.get_stock()}]")
            participants = rg.list_participants()
            if len(participants) == 0:
                print("Er zijn nog geen deelnemers geregistreerd!")
            for participant in enumerate(participants):
                print(participant)
        elif selection == 4:
            participant = input("Wie heeft bier gekocht? Vul hier zijn naam in: ")
            number_of_crates = input("Hoeveel kratjes heeft hij gekocht? Vul hier het aantal in: ")
            while not number_of_crates.isdigit():
                number_of_crates = input("Het aantal kratjes is geen getal. Probeer het nog een keer: ")
            number_of_crates = int(number_of_crates)
            if rg.register_beer_crate(participant, number_of_crates):
                rg.save_data(BEER_FILE)
                print("Kratje(s) succesvol geregistreerd!")
            else:
                print("Er is een fout opgetreden!")
        elif selection == 5:
            participant = input("Wie heeft bier gedronken? Vul hier zijn naam in: ")
            amount_of_beer = input("Hoeveel flesjes heeft hij gedronken? Vul hier het aantal in: ")
            while not amount_of_beer.isdigit():
                amount_of_beer = input("Het aantal kratjes is geen getal. Probeer het nog een keer: ")
            amount_of_beer = int(amount_of_beer)
            if rg.register_beer_consumption(participant, amount_of_beer):
                rg.save_data(BEER_FILE)
                print("Bier consumptie geregistreerd!")
            else:
                print("Er is een fout opgetreden! (Sorry eindgebruiker. Neem contact met uw systeembeheerder.)")
        elif selection == 6:
            print("Wie is de Bock?")
            participant = rg.get_bock()
            print(participant)
        elif selection == 7:
            print("Exporting...")
            he.export(rg.participants, rg.get_stock())

    print("Bye now!")

if __name__ == "__main__":
    main()
