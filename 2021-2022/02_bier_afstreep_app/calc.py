
def add(number_1, number_2):
    return number_1 + number_2

def subtract(number_1, number_2):
    return number_1 - number_2

if __name__ == "__main__":
    print("Testing the calc module")
    result = add(1,2)
    print(f"add(1,2) should be 3: {result == 3}")