import hashlib
from os import minor
import random
import copy
from secrets import choice


salt = 'juup astley'
DIFFICULTY = 5


class BlockChain():
    def __init__(self, genesis_block):
        self._genesis_block = genesis_block
        self._head = genesis_block
        self.difficulty = DIFFICULTY

    @property
    def head(self):
        return self._head
    
    def add_block(self, block):
        self._head = block

    def __str__(self):
        result = ''
        current_block = self._head
        while current_block != None:
            result =  str(current_block) + " <-> " + result
            current_block = current_block.prev
        return result


class Block():
    def __init__(self, prev_block, hash=None):
        self._transactions = []
        self.prev = prev_block
        self.hash = hash
        if self.hash is None:
            self.hash = str(random.randint(0, 100))

    @property
    def transactions(self):
        return self._transactions

    def add_transaction(self, transaction):
        self._transactions.append(transaction)

    def __str__(self):
        result = f"Block: {self.hash} ({len(self._transactions)})\n"
        for t in self._transactions:
            result = result + f"* {str(t)} \n"
        return result


class Transaction():

    def __init__(self, sender, receiver, amount):
        self._sender = sender
        self._receiver = receiver
        self._amount = amount

    def __str__(self) -> str:
        return f"{self._sender} maakt over {self._amount} naar {self._receiver}"


class Miner():

    def __init__(self, name, blockchain):
        self._blockchain = copy.deepcopy(blockchain)
        self.name = name
        self.current_transactions = []

    def add_transaction(self, transaction):
        self.current_transactions.append(transaction)

    def find_hash(self):
        RANGE = 1000000000000000000
        number = random.randint(0, RANGE)
        word = self._blockchain.head.hash + str(number)
        hash = hashlib.sha256(word.encode("UTF-8")).hexdigest()
        last = hash[-self._blockchain.difficulty:]
        if last == "0" * self._blockchain.difficulty:
            print(f"{self.name} heeft gevonden: {hash}")
            block = Block(self._blockchain.head, str(number))
            for t in self.current_transactions:
                block.add_transaction(t)
            self.current_transactions = []
            return block
            # return str(number)

        
    def verify_block(self, block):
        if block:
            word = self._blockchain.head.hash + block.hash
            result = hashlib.sha256(word.encode("UTF-8")).hexdigest()
            if result[-self._blockchain.difficulty:] == "0" * self._blockchain.difficulty:
                for t in block.transactions:
                    try:
                        self.current_transactions.remove(t)
                    except ValueError:
                        pass
                self._blockchain.add_block(block)
                return True
        return False


class Wallet:

    def __init__(self, name):
        self.name = name
        self.transactions = []

    def send(self, receiver, amount):
        transaction = Transaction(self.name, receiver, amount)
        self.transactions.append(transaction)
        return transaction
    
    def receive(self, transaction):
        self.transactions.append(transaction)


def main():
    # with open("video.mp4", "rb") as f:
    with open("07_blockchain/video.mp4", "rb") as f:
        genesis_hash = hashlib.sha256(f.read()).hexdigest()
        genesis_block = Block(None, genesis_hash)
        blockchain = BlockChain(genesis_block)
        minors = [Miner("Steve", blockchain), Miner("Bob", blockchain)]
        wallets = [Wallet("Alice"), Wallet("Eve"), Wallet("Anna")]
        miner_index = 0
        odds_transaction = 100000
        while True:
            if random.randint(0, odds_transaction) == 1:
                sender = choice(wallets)
                receiver = choice(wallets)
                transaction = sender.send(receiver.name, random.randint(0, 20))
                receiver.receive(transaction)
                for m in minors:
                    m.add_transaction(transaction)
                print(transaction)
            block = minors[miner_index].find_hash()
            if block:
                #TODO: Verify
                for m in minors:
                    if not m.verify_block(block):
                        print("Invalid block")
                print(minors[miner_index]._blockchain)
            miner_index = (miner_index + 1) % len(minors)


if __name__ == "__main__":
    main()
