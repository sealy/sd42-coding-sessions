from PIL import Image

# Valid step sizes: 2, 4, 5, 10, 20
STEP = 5
OPACITY = 255

def main():
    im = Image.open('convert.png', 'r')
    width, height = im.size
    pixel_values = im.load()
    for y in range(0, height, STEP):
        for x in range(0, width, STEP):
            block = []
            for i in range(STEP):
                for j in range(STEP):
                    block.append(pixel_values[x+i, y+j])
            avg_red = sum([pixel[0] for pixel in block]) // len(block)
            avg_green = sum([pixel[1] for pixel in block]) // len(block)
            avg_blue = sum([pixel[2] for pixel in block]) // len(block)
            for i in range(STEP):
                for j in range(STEP):
                    pixel_values[x+i, y+j] = (avg_red, avg_green, avg_blue, OPACITY)
    im.save(f"converted_{STEP}.png")

if __name__ == "__main__":
    main()