from crypt import methods

from flask import Flask, render_template, request

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True


@app.route('/')
def hello():
    return render_template('index.html')

@app.route('/search', methods=["GET", "POST"])
def search():
    data = request.form.get('keyword', None)
    print(data)
    return render_template('search.html')
