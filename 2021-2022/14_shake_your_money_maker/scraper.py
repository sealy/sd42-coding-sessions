import requests
from bs4 import BeautifulSoup

URL = "https://www.lookfantastic.nl/health-beauty/make-up/eyes/eye-shadows.list"
page = requests.get(URL)


# Class die we zoeken  productBlock
soup = BeautifulSoup(page.content, "html.parser")
products = soup.find_all("div", class_="productBlock")

print(len(products))
# product_title = products[0].find_all("span", class_="visually-hidden")
# print(products[0])
# print(product_title[0].text)
for index, p in enumerate(products):
    title = p.find_all("span", class_="visually-hidden")
    print_title = "Unknown"
    if len(title) == 1:
        print_title = title[0].text.strip()
    print(f"{index+1}: {print_title}")