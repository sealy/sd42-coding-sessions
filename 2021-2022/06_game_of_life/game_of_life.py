import copy

HEIGHT = 20
WIDTH = 3 * HEIGHT

DEAD = " "
ALIVE = "*"


def init_universe(width, height):
    return [[DEAD for _ in range(width)] for _ in range(height)]

def print_universe(universe):
    for w in range(len(universe)):
        for h in range(len(universe[0])):
            print(universe[w][h], end="")
        print()


def parse_tuple(line):
    result = None
    line = line.strip()
    if line.startswith("(") and line.endswith(")"):
        parts = line[1:-1].split(",")
        result = (int(parts[0]), int(parts[1]))
    return result


def save_universe(universe, filename):
    width = len(universe[0])
    height = len(universe)
    with open(filename, 'w') as f:
        f.write(str(width)+"\n")
        f.write(str(height)+"\n")
        for h in range(len(universe)):
            for w in range(len(universe[0])):
                if universe[h][w] == ALIVE:
                    f.write(f"({h},{w})\n")
    

def read_universe(filename):
    universe = None
    try:
        with open(filename) as f:
            height = 0
            width = 0
            for index, line in enumerate(f.readlines()):
                if index == 0:
                    width = int(line)
                elif index == 1:
                    height = int(line)
                else:
                    if not universe:
                        universe = init_universe(width, height)
                    coord = parse_tuple(line)
                    universe[coord[0]][coord[1]] = ALIVE
    except:
        pass
    return universe


def get_neighbors(universe, cell_position):
    result = []
    for y in range(-1, 2):
        for x in range(-1, 2):
            if x == 0 and y == 0:
                pass
            else:
                new_x = cell_position[0] + x
                new_y = cell_position[1] + y 
                if  new_x >= 0 and new_x < len(universe[0]) and  new_y >= 0 and new_y < len(universe):
                    result.append(universe[new_x][new_y])
    return result
    
def apply_rules(universe):
    duplicate_universe = copy.deepcopy(universe)
    for h in range(len(universe)):
        for w in range(len(universe[0])):
            neighbors = get_neighbors(duplicate_universe, (h, w))
            num_neighbors = neighbors.count(ALIVE)
            if duplicate_universe[h][w] == ALIVE and (num_neighbors == 2 or num_neighbors == 3):
                universe[h][w] = ALIVE
            elif duplicate_universe[h][w] == DEAD and num_neighbors == 3:
                universe[h][w] = ALIVE
            else:
                universe[h][w] = DEAD
