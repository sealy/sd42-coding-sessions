from game_of_life import read_universe
import ui

UNIVERSE_FILE = './a_game.txt'

if __name__ == "__main__":
    ui.render_universe(UNIVERSE_FILE)
