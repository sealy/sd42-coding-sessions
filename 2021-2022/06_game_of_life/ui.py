from os import dup
from re import S
from  game_of_life import init_universe, read_universe, save_universe, apply_rules, DEAD, ALIVE
import pygame, time, copy, random


DEFAULT_WIDTH = 10
DEFAULT_HEIGHT = 10
DIM_CELL = 10
BACKGROUND_COLOR = (255,255,255)
SHAPE_COLOR = (0,0,255)
BORDER_COLOR = (128, 128, 128)

def draw_universe(screen, universe):
    screen.fill(BACKGROUND_COLOR)
    for h in range(len(universe)):
        for w in range(len(universe[0])):
            x = w * DIM_CELL
            y = h * DIM_CELL
            pygame.draw.rect(screen, BORDER_COLOR ,(x, y, DIM_CELL, DIM_CELL), 1)
            if universe[h][w] == ALIVE:
                pygame.draw.rect(screen,SHAPE_COLOR ,(x, y, DIM_CELL, DIM_CELL))


def _render_loop(universe):
    screen = pygame.display.set_mode((len(universe[0]) * DIM_CELL, len(universe) * DIM_CELL))
    pygame.display.set_caption('Game of life')

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                break

        draw_universe(screen, universe)

        pygame.display.flip()
        time.sleep(0.1)
        apply_rules(universe)

def render_universe(universe_file):
    # Initial window        
    print("Creator mode")
    universe = read_universe(universe_file)
    if not universe:
        universe = init_universe(DEFAULT_WIDTH, DEFAULT_HEIGHT)

    screen = pygame.display.set_mode((len(universe[0]) * DIM_CELL, len(universe) * DIM_CELL), pygame.RESIZABLE)
    pygame.display.set_caption('Game of life - creator mode')
    screen.fill(BACKGROUND_COLOR)
    draw_universe(screen, universe)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                break
            elif event.type == pygame.VIDEORESIZE:
                width, height = event.dict["size"]
                print(f"Resizing: {width // DIM_CELL}, {height // DIM_CELL}")
                duplicate_universe = copy.deepcopy(universe)
                universe = init_universe(width // DIM_CELL, height // DIM_CELL)
                for h in range(len(universe)):
                    for w in range(len(universe[0])):
                        if w < len(duplicate_universe[0]) and h < len(duplicate_universe) and duplicate_universe[h][w] == ALIVE:
                            universe[h][w] = ALIVE

            left, _, _ = pygame.mouse.get_pressed() 
            if left:
                # print("Left Mouse Key is being pressed")
                # print(pygame.mouse.get_pos())
                x = pygame.mouse.get_pos()[0] // DIM_CELL
                y = pygame.mouse.get_pos()[1] // DIM_CELL
                if  universe[y][x] == ALIVE:
                    universe[y][x] = DEAD
                else:
                    universe[y][x] = ALIVE

            if event.type == pygame.KEYDOWN and event.key == pygame.K_r:
                for h in range(len(universe)):
                    for w in range(len(universe[0])):
                        if random.randint(0, 7) == 0:
                            universe[h][w] = ALIVE

            if event.type == pygame.KEYDOWN and event.key == pygame.K_s:
                save_universe(universe, universe_file)
                print("Save universe")
                break

            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                print("Play universe")
                _render_loop(universe)

        draw_universe(screen, universe)
        pygame.display.flip()



def stop():
    pygame.QUIT