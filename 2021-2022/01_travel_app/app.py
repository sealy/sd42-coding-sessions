from os import name, write
import yaml
from db import read_countries, read_regions

def print_menu():
    print("""[T-Reizen]
1. Toon bestemmingen
2. Maak map
3. Maak layout
0. Afsluiten""")


def create_feature_list(regions):
    features = []
    for region in regions:
        features.append(create_feature_json(region))
    return features


def create_feature_json(region):
    return f"""{{"type": "Feature","properties": {{"name": "{region['name']}"}}, "geometry": {{ "type": "Point", "coordinates": [{region['longitude']}, {region['latitude']}] }} }}"""

def generate_html_map(country = None):
    fc = "{}"
    center = [-1.2649062, 52.5310214]
    if country:
        regions = read_regions(country['id'])
        features = create_feature_list(regions)
        features = ", ".join(features)
        center = f"[ { country['longitude'] }, { country['latitude'] }]"
        fc = f"""{{
  "type": "FeatureCollection",
  "features": [{features}]
}}
"""

    return f"""<!DOCTYPE html>
<html lang="en-US">
<head>
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.3.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.3.0/mapbox-gl.css' rel='stylesheet' />
</head>
<body>
   <div id='map' style='width: 400px; height: 330px;'></div>
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoibmV0bW9iaWVsIiwiYSI6ImNrN2JuNGNseTA5c2szbm4yeGU5d2themEifQ.eJX3zGnMvE8dU1dFuitKfg';
        var map = new mapboxgl.Map({{
            container: 'map', // container ID
            style: 'mapbox://styles/mapbox/streets-v11', // style URL
            center: {center}, // starting position [lng, lat]
            zoom: 5 // starting zoom
        }});
        map.on('load', function () {{
            // Load an image from an external URL.
            map.loadImage(
                'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN4AAADjCAMAAADdXVr2AAAAh1BMVEUAAAD////+/v7t7e3s7Oz39/f6+vr9/f309PTv7+/y8vLr6+v19fXg4OCPj4/o6Oi/v7/R0dHLy8sfHx/Z2dmjo6OFhYViYmJJSUlnZ2dtbW20tLStra2ZmZlEREQ/Pz+Kiop8fHwmJiZaWlqenp4zMzMWFhbDw8N0dHQtLS1OTk4ODg45OTmpzuazAAATCklEQVR4nNVdaX/bLAwPtrGBkqNpeqxHmq7duvbZ9/98j/ERbAwS4KMdb8YPVlty0IEk/qySshGRlk2SsltkZS8r1KBUg0INkmowL7uEqkFeDTJknqtBSlSj8mJ9WF/u3993D7v3/aEalkzwlKo/ytXfp+RMCdWUJOdHdf5rpt/PVDexEV09YHVmL2MGeywzyFc9mmn2kHnOBZOJuHzc3P9dDdrP15enh8NFxb3BXtb50EnS+VJlt8OelWiZLcKe+sm2u83r1ZCxXjve7EoWBf+X2BMlc5ebZ4Szc7u6f1xLxmdhr1odIsuyC6Z6xYXqFqrLVFdU89VgXkmR6nICzKeCbh9++LLWtr+by+pJXD1JakoSPVqJcV6NnonOKqITTbTU9KkHrIRqNC8bVT2mejnrDwo9KKuucM7nhD68hvJWt59P67x6ZpcSoZ9vjlrfT/W8+q+rVCu98sfMGq2QNYuhq6lE1iwGUi8Gy7xgly9xvNXt7rFcAn1KKvoK0qy7bKCp3USr0YY9U+n5rHWaGPOMP1pUZGDbpMSkZCBW5Ex04iY6nZS9UkA2o3mr2v26ev9E7GmtkJ2lUoly5hblIuvPZ6rzNA1zqr3uKe9S0miN7KzKMoTorB1dSVY2WjXVk1XPGOx0qW1eFvRtOuZUuz2Q5k0+9J0H5Zk+WY+uzB+mYxj017IbBj1//d+03JXthYm0ZxiA1dQ1DJqoyjCMNuuEHLwNeFB7Y/wbeC3FKFMAtV+XNJ2GvczBntXOdNjjxX4u5lR7qd7kYs9GtDaONXuFaoyXTaperno8V12peqya14O06ibNPMuCva+wdnUg6lVCvZRq+jTRJn0V0SJp/+sqs2kNp4/XdUzL+cPvebkr24nxoTtpEJ3aiK4NQ7xZz8mEps7djlvxFV4LZ5Guc3B7l8uzxy6nt3Wu9kRILHuRskcfFmOubC/RsmdozkJrJq05k4HmJNdLcrdafXLhpzmpoTnj7N5pWe5Wq48tj7J7EV4Lp3+W5m61+n3Il3HKOLtdnruyrUeFknx3DOliBsFs24gdg/9+rxkkX/PbqbamyH6vtx+t9nuBu3VJvkDu2vbfmuvdeua1Ww8z6ym5GUHe749ff39hcWuoXW3ZrF4LjXIzP+6fdpdpVhokUUv1/uHpT9Qe+DmOPc84Jwv3VZ5Pu3W1jGqa6gB0ySeT+frxPvhxt5V/5h/nDIlS00MoMQ8XuYrrGKHlupuXv2RxeQrcU20IHqWmOkpt3co5cgx5kBd9fLygPNc+IDEc27TRD4ewaMYDC8kxBJh18hlAxc2WCj4MZQ9kRQWA6dvPgCcf+CxeCwmIQj+VX7UlH2OvnBfi4Zf3s3/ydAb2hH/Q6IaqREgAeyr58uD9C97T6Z2y9ML37a9rat1/2WSvMy/8N1kPiOx1nDLv7axnTOxq38s8mvrJzEx25jO29fX31nziUBLZ+b34RtklQIDN8otqvi1v4L4xgFc6rddCqN97dwmin0D2yv/puR15ZNOy5+VI322FlXx/9rKC+OWatnzKUNKlzytfGIdkyy57w02l17vuqZ/sWasCqp7UThr98HjjI/HI73vk/+nax087JFaiO89Xo6t+VKapCqhS9Z1Qks+C2REfv9ynPoFzj93EcyeUlGiiO/UFvmadFB7c7ZldgD3NetqtSqLJncfnpBN5LT7e2J459FMMe6WA4u7tTy/2PLwW5sUdFtFovBZ38EBTovTPEX3ntfAIJbWp9jbrXva63fIfj/jDPu/9URPakZZBbL4dzCnqgv5HHUQz/apuKMluGPga/479D+9Q/Bky34k2KkLw1z4OiQ4PJbET9pob2hWbcWZdUyLR0MBVuSzHei34TuFT2ksxR7LHCeqA7nzZIw72SC5Qm7flnZSGjfzMzl5im++yR7AoxfOg/GzAnpnbKoxUPSrie9b9I2lkqcz6g4AsVlEQ7N0HYieaNw8osFCSxDzAk6EVkByoezub6T9q9Q/HXn7PRoaSciQU+bMTd5zQrDeU0BPC33ZQvh/ktXBMseztYjMRe1wgy/M6H8Uepljui1nZSwWyPJ8x9mDZY4hvtGamRXbKHhZKssieElgkxLNGZM+mOZNWyRFkbT6RXBdheWlOcx7UnCUlAjHuT7L/flNzgnaPPMIPH5aST2n3KkqQPP5fOcJrQfYlb2SQ657Sa6kowXzPA49mD9sKFQuwl1LYNF0L31LxoVaAg5ubvlbw2TH4hJJ6m8IUs+2v0hJK0jsGqDQcKbRdU0c9OVZvjtcndAcRCXE+X3VX1WEtx25dgnnwVwp/+MFuPTF/GMtunluWALyGDrbdeuLhlHFYKT+I1F61NKFZV+q1XGJQuyaRXouAN1yD05kzsZfA0ZBbkD1tsgZxTgaK3kthmqzWeMbFOdO+8ST6cCkSty4XpybajHNaz4bVUeocjKa+586AM1B/EHpKreoVYE5/WwBR6gwwDOBHuzCrAoSP4vczDEZ9AQM9lwceld/Lt9BDX+XggNs8Zl39Pag7TyzKayneoYe+ieXY4wKi5FZGsZeAe739sDp2NvYyAmWofkG/nlv24EAVH1jkWWSvPr8HkyIG+0vtlGnKOlGZ6s0F5Azd4Sejx4eS2j8iDCyaSIm7MsJt1nPIJXshOOrAVGa9XMKgGrgkMV4LWC3wuCh7Atz07aLYAwMR+0XZ4xKi5Rpgz5C9zo4BdKi3wbIXF0pqDpdK6MT4xpQ9N+qAdo0kWEO2ZSgqgVl/MKjntM1bPTeRE2jL/pJb/TkYdYCDrgJ1oBJ0vGFnKIl0NgPOoy4G6gBkGW6p1YWHzTq8HaIW1IH5zHoBHhV8pRFei4CCgB9LswcZvjuIPVccQUBPfKbDOIIZSjKrJjMk1OSOaJSPgr71URdYDlEHdNZd9hL4OcTeZzIsBaD9HjboNy/rbgGyl1j/HkEdANlzfvgJDMPgJyYcXErHrmHQRCFOGfTB7ogHFNSEZh1k7y7Ka4HY+7s0e9DmDGcvs7AHGYYr4pH9aDWZGUqyzxuhJIM90DAY7HmhDsDRUwKhEghrqUCCzFvz/0U9SqFY4C3R75f6+TDqAAc3ITwdaAX7dja8XHUYSiq1BhRMupdWVQaHkuDUxZova9ahEusXGeG1wIm1/cLsQVvrDYtgLwWTe49iWfYgWq4B9pyyl4E1uBthRyUIlj1k01jLHgFDrjvhlj036kABbSF/EEhzJt36A4vmdM4nVs2ZSHD3sia292OoAwUkzldkSbtHwXLnHLB7bq8lB/NOYkmvhYIJWpL02fNzysAtw+p90UAgRMlnNzrgXypOQbt+itwxZFnEjgE0wS+RqAMceuiv3CPrH1RK4P4jGKDvOrc/H0UdALOGhzq/l022W89clKQSPLXxziNLxcFytbdiKbOOFCZteWRVEqhbjoQsxB64l1VRrUjUAbjwY2uYtNninBRcm39YLOoAfOTy6XygDcj6e0WprYC7Z0oYUl5DHaUKKOoA/Nl+K4TJJXIMMAzHRXSpOIMfrMqS5jfrSFHSB4mupebwSfw7ugh7cG3UaQR7HD7muec4e2QkewQ5q7GH2XM7ZQyrl1E1gUj8fAKnDDkwr/4r4JQ5t7PVmxGciMP4UBKWA2Xwj/dDuT2xqAOIWJfO+txmHa6JUDt1lzD4HHDDDmG9k5nZy+D3r4bXWwSxh5wCucrTedlDDra+EKcq80IdgHcNpV6WI0NJ8KYRO2F6qB4VjzqQY6cTDwmY9R9Xz5kjGDw/8zDUgcEBEgYW463qE25zVeOi59vexGjUAQxL7IbM5bXg8EVbPhp1AD2A/E7mYY9vMdywFzkB6gCK37D1PMfgnrdGNBj64gP3RB0Asvo5GDBT7YpZCwDcDwXmdQ8997z6kaBVCTjqQMrQG0CezQ/vDBX5F354gC2uh0RHAFgipzWq76jU56RmHc4qNC9NJsFKAsvx6vZH/XoTsic9gLXW/uwRB3uVSRKo9FX8TXj60oe7H8RFNIA6IGwJ/NwDXeuemPUH1rOzXqgD2JnWqq0BqAR9dla7iwCApQ/41GcB6KezY+oTSqI+aHqbQX17PICl9IHQ/CvIFGadSx/43f/IhAiPnviHlxYEjlD2xIUXCuj7lOxlHsZBtbfaOx7BXr73QnC9JQ6iIy+ok37Yfa8XDAklIbLniZwsyPBsifWYhqE5uSOr7wN3VbWH8rmxqAPU90aja2kheqA5kzPqAGjW6yvMPBEsV5/bbq47wO4J7gvp/UpBYx0Hu+0NIn2ijAd7LcTL2NUt5y3RU6KK+8Odby4YD2KPkL3/1X37We5Cgc9tGO3UQJ34oA7wnOwCINQ3+F0oescQkNUPWD5lu7/Mc4qXEuQFuXgKgcE/5tKvVAFFHRhsxcKuxPrYHGjSUfwD1IFU3T+wC7zfYc09rpvwQR0wxCYv/yb0UqyPzWX5V6WhMc26+nxSHq6Db+bYsV7YeNK7UNCwqq19Pu0OF1LWspgrMH8lIOLy7UfERU0nmgax149zOtBVs7OdCRK/Tvt4fXl63KkrrR+uNzf3x8i70e66OY2WaCjOGZj1DxS/qVuaGwFtLErtbxgqH48mYy77GNt2YlDAhOQYAsx6vdZjxG+idkPnv8EtWvxGtyPj87OXfJn4rfmEtwYDF4B/jfjtRIYWDwagDjiz+iiu5CzththUHbadDTLrzQqmXyB+R7bgtbrhFwiNbWs7wsg87C0ufjsWd2uwsYzBe4g6tn9h63dDobs4gTAuksB3oQrApwCmbkefuxDCUAfsqALnxUCXvIaPG95y5kN0pFlvYGuz5cRvR/pitcitwaH3nUW3mzG3BrscAOB2+qLyCpZyPp9ZZlYFOL0WK+pAQNa/O7jQLZgpDQAo6AwiqAPYGSGWpkuI3x5fTUDhR5xZr+cx1Ogp2oZ46oIpvZZmXvrdiTSi3ZEp2Msc7LnufG7nZ7d+AtDkNqK9UQdQVIF6fl7x2xE3FoF54DcMdQC+b72dT2YVv80ZdYBE3rcebdabvDGbUfyOpD23vrzX0qbFZxQ/9g3Y47NZvz1JRrM3UvZyjys9IpsCLR8te9WpfivqQK0ZdSmBkarX8/M4n3dSow7YNGdirdeS3qgDGKpAZ34O8RM1xGsyzu6N8lrOsjC9+O3rl0Lsze6UtfPTi9+mc8B2DHsjdgyd+amt36eBUwZHzYEdw6i7BHR34sjnlnYpwQAK7PR1UQeqdVf9MGWv3V+pqEz9tWpRrr5WlffU8/XGPSGT5v1UOqFDyUWTeK1+GDVavRQgOmtHx5v1Zn7KyGeVQO+BCX2d13KenyzyeUfSidlDQoZ9O9MoNXM+n8r6sbxDSc0eiY1zQqgDcBTYnJfYhV2e7Z2YlGBn4yJRBxDDMJifRPxuqAXZ2N8wxJSKY2a9mZ/C+TwyGwTi13ot7fwE1m/NvzF7o/N+6qD2lOzpZQzgAFnP51nnRyYeNrlBiZa9uDDu+O1s1kUdADF60XZk5lGWrw4lGfNylPituQMC8Rt4Le0Bt3jx29mv/f5W7MVbvxvqBLCMZm8K2TNQByLF71laKBkre+7SSGs9J3XPnz2nIs75FIWrstQ+6lXP6Y7KYBhbznnOY5zPnabEuAMMLWUYgzoQYtabeRYhfo5bzb+V13KeDxa/0tWchT3DARjrtbTzodZvzYELNEd4LQGlAiHzeVip+z73hCrwej89d3HUAcvXqkJJVqfs/OHDIp8b5xJpQ0nEVfgxGnUg1Gup52VA6KVKoIO41N/Ha2nnqX/J9VZr93nYIw723CVzwEU99Xxa+DqfO+pWzwZ7UClDNOqAmarP3QACnXlf5/NEsVsP7KPToQ54hpIMUccg1Op253NJFuwDLxRKMheLV9qdEY/LQr6Z11LT5JN2VwiN/yh7KV50ttGqYF6nbHrZU14AIn7H7uHTOWTPVhVgv0ug1ozJWXNZ7xqo5jv5/QQWP0HP9QnArQdmVYC1lGEc6kCw3WsWCwjQuve69WBo92BjvYjX0ggw5HxuiO9lISO9lvnYy9zO5/mmrRnZCwslee8YMo0qIF3iJ4j/PUSk2TH4hZL0jgGpCgDuEvC6a6D8N6f20Mt77l2fEFjK0EMdSHpVAY2OrdZVljWi3HzYpJOq78wT+7xO5Vudzw09z7eoBO0fSU1JYjyqXU09ohNNtNREz+6UtQJqS7vfhVwW8k29lpZ8i/PJlmGvb9JQ1AGfOOcwlZ8ODhztiQe6ajo+zumOAruj1EzPw3cNnAepkfc7Ec/6BDNKjV3jOxJ1AMvvOQEqe5FPlUAPu6CO9E79YoZhxvyeQ4B7kc91H0vpX/ZaWvY6WC97kf4D7NnmAXzOs/O5Idly7OllnGWOiGjHKZNnpwxC7c2Gsld231qLF3FrMIk8v2doDR/UgYDtbF//3DeCF37fOqzqJkYdCDbr9QomiXI+dwIGsPwHvZaGJuV83lAEn/PfZS8hj78Uvt5XsJcO2EsN8iunK9XsIfOV0jM9KQXpZY/0a/ZS46opx6OsRMu0x97/rDuC+p3kqzUAAAAASUVORK5CYII=',
                function (error, image) {{
                    if (error) throw error;
                    
                    // Add the image to the map style.
                    map.addImage('marker', image);

                    map.addSource('marker', {{
                        type: 'geojson',
                        data: {fc}
                    }})
                    map.addLayer({{
                        'id': 'layer-marker',
                        'type': 'symbol',
                        'source': 'marker',
                        'layout': {{
                            'icon-image': 'marker',
                            'icon-size': 0.1
                        }}
                    }})

                    map.on('click', 'layer-marker', function (e) {{
                        var coordinates = e.features[0].geometry.coordinates.slice();
                        var description = e.features[0].properties.name;
                        
                        // Ensure that if the map is zoomed out such that multiple
                        // copies of the feature are visible, the popup appears
                        // over the copy being pointed to.
                        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {{
                            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                        }}
                        
                        new mapboxgl.Popup()
                            .setLngLat(coordinates)
                            .setHTML(description)
                            .addTo(map);
                    }});
                }}
            )
        }});
    </script>
</body>
</html>"""

def write_to_file(file_name, content):
    f = open(f"export/{file_name}", "w")
    f.write(content)
    f.close()

def generate_html(layout, data):
    print(layout)
    for item in layout['layout'].items():
        html = generate_html_element({ item[0]: item[1] }, data)
    return html

def generate_html_element_with_class(key):
    parts = key.split('.')
    if len(parts) == 2:
        return f'{parts[0]} class="{parts[1]}"'
    return f'{parts[0]}'


def generate_html_element(item, data):
    print(item)
    html_element = ""
    for key, value in item.items():
        
        # Split the keys on dot for css classes
        key_element = key
        if "." in key:
            parts = key.split('.')
            key_element = parts[0]
        
        # Parse list-item values
        if isinstance(value, str)  and value.startswith('list-item'):
            data_key = value.split("'")[1]
            value = data[data_key]

        # Process list (in yaml)
        if isinstance(value, list):
            html_element += f"<{key_element}>"
            for v in value:
                html_element += generate_html_element(v, data)
            html_element += f"</{key_element}>"
        else:
            # Create the different html elements
            if key_element in ["body", "main", "header", "section", "div"]:
                html_element += f"<{key_element}>{generate_html_element(value, data)}</{key_element}>"
            elif key_element in ["h1", "h2", "h3", "p"]:
                element = generate_html_element_with_class(key)
                html_element += f"<{element}>{value}</{element}>"
            elif key_element.startswith('list'):
                data_key = key_element.split("'")[1]
                data_values = data[data_key]
                for data_item in data_values:
                    html_element += generate_html_element(value, data_item)
    return html_element

def generate_layout(layout, countries):
    # Read template
    template = read_yaml(layout['template'])
    css_file = layout['css']
    print(f"Template: {template}")
    # Generate pages
    for (key, value) in template.items():
        file_name = f"{key}.html"
        css_link = f'<link href="{css_file}" rel="stylesheet" />'
        data = { 'countries' : countries}
        body = generate_html(value, data)
        html = f"""<!DOCTYPE html><html lang="en-US"><head>{css_link}</head>{body}</html>"""
        write_to_file(file_name, html)

def read_yaml(filename):
    with  open(filename) as yaml_file:
        parsed_yaml_file = yaml.load(yaml_file, Loader=yaml.FullLoader)
    return parsed_yaml_file    


def main():
    conf = read_yaml('t-reizen.yaml')
    print_menu()
    choice = input("Maak een keuze: ")
    countries = []
    while choice != '0':
        if choice == '1':
            countries = read_countries()
            for index, country in enumerate(countries):
                country['regions'] =  read_regions(country['id'])
                print(f"  {index + 1}: {country.get('name')} - {len(country.get('regions'))}")
        elif choice == '2':
            if len(countries) > 0:
                countries = read_countries()            
                html_content = generate_html(countries[0])
                write_to_file("test_map.html", html_content)
            else:
                print("we have no country boohoohoo")
        elif choice == '3':
            generate_layout(conf['layout'], countries)

        print_menu() 
        choice = input("Maak een keuze: ")

if __name__ == "__main__":
    main()