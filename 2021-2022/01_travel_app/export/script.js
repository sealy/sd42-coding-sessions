function toggle(id, showSummary) {
    // console.log(`Id: ${id}, ShowSummary: ${showSummary}`)
    elem = document.querySelector(`p#${id}`)
    
    if (showSummary) {
        // Laat de summary zien.
        // Verwijder 'hide' class van 'full_summary'
        summary = findElement(elem, 'full_summary')
        summary.classList.remove('hide')
        // Voeg 'hide' class toe aan 'intro'
        intro = findElement(elem, 'intro')
        intro.classList.add('hide') 
    } else {
        // Laat intro zien.
        // Verwijder 'hide' class van 'intro'
        intro = findElement(elem, 'intro')
        intro.classList.remove('hide')
        // Voeg 'hide' class toe aan 'full_summary'
        summary = findElement(elem, 'full_summary')
        summary.classList.add('hide')
    }
}

function findElement(elem, className) {
    for(i = 0; i < elem.children.length; i++) {
        let child = elem.children[i];
        for( j = 0; j < child.classList.length; j++) {
            if (child.classList[j] == className) {
                return child
            }
        }
    }
}