# Reis bureau

## Backend
- [ ] Invoer bestemmingen:
    - [x] Simpele bestemmingen toevoegen.
    - [ ] Keuze uit een lijstje van landen.
    - [ ] Type bestemmingen toevoegen
    - [ ] Automatisch tekst aan details pagina toevoegen
- [ ] Data in een database opslaan in plaats van een CSV bestand
- [ ] Automatisch plaatje van google ophalen wanneer deze niet aanwezig is voor een land.
- [ ] Boeken van een reis
- [ ] Betalen van een reis

## Frontend
- [ ] Fancy look and feel
    - [x] Achtergrond plaatje in HTML op basis van type bestemming. 
    - [x] Bestemmingen groeperen per land.
    - [x] Details pagina voor een bestemming. 
    - [x] Warme landen een zonnetje als icoon, koude landen een sneeuwvlokje 
    - [x] Overzicht pagina opleuken.
    - [x] Knoppen aan bestemming toevoegen (bekijk of meer details)
    - [x] Tekst in header wit met drop shadow  
    - [x] Show/hide summary 
    - [ ] Punaise op de kaart!
- [ ] Zoeken naar bestemmingen
- [ ] Vergelijken hotels/bestemmingen


## Miscellaneous
- [ ] AI bouwen die CSS voor Robin fixt.
