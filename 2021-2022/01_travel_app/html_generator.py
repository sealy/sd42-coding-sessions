
AUTEURS = ['Marc', 'Eelco', 'Indy', 'Robin', 'Timothy']
TEMP_CUTOFF = 20


def generate_html_destinations(destinations):
    html_destination = '<div class="destinations">'
    for d in destinations:
        file_name = generate_file_name(d)
        section_class = 'warm'
        if int(d['temp']) < TEMP_CUTOFF:
            section_class = 'cold'
        html_destination = html_destination + f'<section class="overiew border {section_class}">'
        html_destination = html_destination + '<header><h2 class="secondary-color">' + d['regio'] + "</h2>"
        if section_class == 'warm':
            html_destination = html_destination + '<span class="iconify" data-icon="noto:sun-with-face" data-inline="false"></span>'
        else:
            html_destination = html_destination + '<span class="iconify" data-icon="noto:ice" data-inline="false"></span>'
        html_destination = html_destination + '</header>'
        temperatuur_content = f"Bestemming {d['regio']} kent een gemiddelde temperatuur van {d['temp']} graden."
        html_destination = html_destination + f"<p>{temperatuur_content}</p>"
        html_destination = html_destination + f'<a href="{file_name}" class="button primary-background-color">Details</a>'
        html_destination = html_destination + "</section>"

    land = destinations[0]['land']
    dummies = [
        generate_dummy_section('corona'),
        generate_dummy_section(land),
    ]

    #  If we have less than 3 destinations fill with dummy stuff.
    for i in range(3 - len(destinations)):
        html_destination = html_destination + dummies[i]

    html_destination = html_destination + "</div>"
    return html_destination


def generate_dummy_section(land):
    section = ''
    if land == 'corona':
        section = f'<section class="overiew border corona"><header><h2 class="corona">Corona maatregelen</h2></header>'
        section = section + f'<p>Wegens Corona zijn de boek opties op dit moment beperkt!</p>'
        section = section + f'<a href="https://www.corona.com" class="button corona">Meer corona</a>'
        section = section + '</section>'
    else:
        section = '<section class="overiew border info"><header><h2 class="info">Algemene informatie</h2></header>'
        section = section + f'<p>Bekijk hier meer over {land}.</p>'
        section = section + '</section>'
    return section


def generate_summary_html(summary, land):
    fsi = summary.find(". ")
    first_sentence = summary[:fsi+1]

    html = f"""<span class="intro">{first_sentence} <a href="javascript:toggle('{land.lower()}', true)">Meer details</a></span>
    <span class="full_summary hide">{summary} <a href="javascript:toggle('{land.lower()}', false)">Minder details</a></span>"""
    return html

def generate_html(destinations):
    # Group destination to 'land'.
    grouped_destinations = group_destination(destinations)
    html_destination = ""
    css_style_land = ""
    for land in grouped_destinations:
        bestemmingen = grouped_destinations[land]
        html_bestemmingen = generate_html_destinations(bestemmingen)
        country_summary = ""
        if len(bestemmingen) > 0 and "land_summary" in bestemmingen[0]:
            country_summary = generate_summary_html(bestemmingen[0]['land_summary'], land)
        html_land = f"""
            <div class='land'>
                <div class='image {land.lower()}'>
                    <h2 class="secondary-color">{land}</h2>
                </div>
                <p id='{land.lower()}' class="pa-2">
                    {country_summary}
                </p>
                {html_bestemmingen}
            </div>"""
        html_destination += html_land
        css_style_land +=  f".land .image.{land.lower()} {{ background: linear-gradient(0.25turn, rgba(255,255,255, 0.5) 0%, rgba(255,255,255, 0) 50%), url('images/{land.lower()}.jpeg'); background-size: cover; }}\n"
    save_css(css_style_land)

    html_content = f"""
    <div class="content">
        <main class="content_width">
            {html_destination}
        </main>
    </div>"""
    return wrap_in_html(html_content)


def group_destination(destinations):
    grouped_destinations = {}
    for d in destinations:
        land = d['land']
        if land not in grouped_destinations.keys():
            grouped_destinations[land] = []
        grouped_destinations[land].append(d)
    return grouped_destinations


def save_css(css):
    write_to_file("style-landen.css", css)


def export_html(destinations):
    export_main_html(destinations)
    
    grouped_destinations = group_destination(destinations)

    # Generate details pages.
    for d in destinations:
        export_details_html(d, grouped_destinations[d['land']])


def export_main_html(destinations):
    html_content = generate_html(destinations)
    write_to_file("t-reizen.html", html_content)


def wrap_in_html(content):
    auteurs_content = ", ".join(AUTEURS)
    return f"""<!DOCTYPE html>
<html lang="en-US">
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="style-landen.css">
    <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.3.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.3.0/mapbox-gl.css' rel='stylesheet' />
    <script src="script.js"></script>
</head>
<body>
    <header class="primary-background-color">
        <div class="header-content content_width">
            <a href="t-reizen.html">
                <span class="iconify home" data-icon="ant-design:home-twotone" data-inline="false"></span>
            </a>
            <div class="header-text">
                <h1>T-Reizen</h1>
            </div>
        </div>
    </header>
    {content}
    <footer>Deze HTML pagina is gegenereerd (samen met {auteurs_content})!</footer>
</body>
</html>"""


def export_details_html(destination, neighbouring_destinations = []):
    content = f"Bestemming {destination['regio']} kent een gemiddelde temperatuur van {destination['temp']} graden."
    # TODO: Generate content based on wikipedia
    if not destination['regio_info'] is None:
        content = destination['regio_info']
    html_content = f"""
<div class="content">
    <main class="content_width">
        <header>
            <h1 class="secondary-color">{destination['regio']}</h1>
        </header>
        <main class="details">
            <div class="info">
                <section class="border">
                    {content}
                </section>
            </div>"""
        

    if len(neighbouring_destinations) > 0:
        html_content += f"""
        <div class="pa-2">
            <h2>Alle bestemmingen in {destination['land']}</h2>
            <div class="other_destinations"> 
                <div class="destinations">""" 
        for i in range(len(neighbouring_destinations)):
            neighbour = neighbouring_destinations[i]
            file_name = generate_file_name(neighbour)
            html_neighbour = '<section class="border"><header><h2 class="secondary-color">' + neighbour['regio'] + "</h2>"
            if int(neighbour['temp']) >= TEMP_CUTOFF:
                html_neighbour += '<span class="iconify" data-icon="noto:sun-with-face" data-inline="false"></span>'
            else:
                html_neighbour += '<span class="iconify" data-icon="noto:ice" data-inline="false"></span>'
            html_neighbour += f'</header><a href="{file_name}" class="button primary-background-color">Details</a>'
            html_neighbour += '</section>'
            html_content += html_neighbour
        
        loc = destination['land_location'].split(',')
        html_content += f"""
                </div>
                <div id='map' style='width: 400px; height: 330px;'></div>
                <script>
                    mapboxgl.accessToken = 'pk.eyJ1IjoibmV0bW9iaWVsIiwiYSI6ImNrN2JuNGNseTA5c2szbm4yeGU5d2themEifQ.eJX3zGnMvE8dU1dFuitKfg';
                    var map = new mapboxgl.Map({{
                        container: 'map', // container ID
                        style: 'mapbox://styles/mapbox/streets-v11', // style URL
                        center: [{loc[1]}, {loc[0]}], // starting position [lng, lat]
                        zoom: 5 // starting zoom
                    }});
                </script>
            </div>
        </div>"""
    
    html_content += f"""
    </main>
</div>"""
    html_content = wrap_in_html(html_content)
    file_name = generate_file_name(destination)
    write_to_file(file_name, html_content)


def generate_file_name(destination):
    return f"details-{destination['land'].lower()}-{destination['regio'].lower()}.html"


def write_to_file(file_name, content):
    f = open(f"export/{file_name}", "w")
    f.write(content)
    f.close()
