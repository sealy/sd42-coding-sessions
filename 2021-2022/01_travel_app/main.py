from html_generator import export_html
import csv
import wikipedia
from geopy.geocoders import Nominatim

BESTEMMINGEN_CSV_FILE = 'bestemmingen.csv'

def add_destination():
    land = input("Geef de naam van het bestemmingsland: ")
    regio = input("Geef de naam van de regio: ")
    soort = input("Geef de soort vakantie aan: ")
    gemiddelde_temperatuur = ''
    while not gemiddelde_temperatuur.isdigit():
        gemiddelde_temperatuur = input("Geef de gemiddelde temperatuur in de regio: ")
    
    destination = {
        'land': land,
        'regio': regio,
        'soort': soort, 
        'temp': gemiddelde_temperatuur,
    }
    return destination


def pretty_print(destinations):
    for index, d in enumerate(destinations):
        print(f"{index+1}. Land: {d['land']}, Regio: {d['regio']}, Temp: {d['temp']}, Location: {d['land_location']}")


def generate_csv_header(destinations):
    # Go through all entries and combine all keys.
    # If a key is not in the header list add it to the list.
    header = []
    for d in destinations:
        for k in d.keys():
            if k not in header:
                header.append(k)
    return header
            

def save_destinations(destinations):
    if len(destinations) > 0:
        with open(BESTEMMINGEN_CSV_FILE, mode='w') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            # Write the header
            header = generate_csv_header(destinations)
            csv_writer.writerow(header)
            # Write destinations
            for d in destinations:
                row = []
                for k in header:
                    row.append(d.get(k))
                csv_writer.writerow(row)
    else:
        print('INFO: No destinations to write to file.')


def read_destinations():
    destinations = []
    with open(BESTEMMINGEN_CSV_FILE) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        header_keys = []
        for row in csv_reader:
            if line_count == 0:
                header_keys = row
            else:
                dict_item = {}
                for i in range(len(header_keys)):
                    dict_item[header_keys[i]] = row[i]
                destinations.append(dict_item)   
            line_count += 1
        print(f'Processed {line_count} lines.')
    return destinations


def rem_destination(destinations):
    pretty_print(destinations)
    while True:
        selection = input('Kies een bestemming om te verwijderen: ')
        if not selection.isdigit():
            print('Probeer het nog een keer. Kies een getal aub!')
        else:
            selection_int = int(selection)
            print(f'Keuze was {selection_int}')
            del destinations[selection_int - 1]
            save_destinations(destinations)
            break

def update_summaries(destinations):
    countries = {}
    for destination in destinations:
        land = destination['land']
        # Zoek wikipedia info op voor het land (summary)
        if land in countries:
            destination['land_summary'] = countries[land]['summary']
            destination['land_location'] = countries[land]['location']
        else:
            countries[land] = {
                'summary': '',
                'location': ''
            }
            result = search_wikipedia(land)
            if result is not None:
                countries[land]['summary'] = result.summary
                destination['land_summary'] = result.summary
            geolocator = Nominatim(user_agent="https://nominatim.openstreetmap.org/")
            location = geolocator.geocode(land)
            countries[land]['location'] = f"{location.latitude},{location.longitude}"
            destination['land_location'] = f"{location.latitude},{location.longitude}"

        # Zoek wikipedia info op bestemming
        destination['regio_info'] = ''
        result = search_wikipedia(destination['regio'])
        if not result is None:
            destination['regio_info'] = result.summary



# def fetch_wiki_data(destinations):
#     for destination in destinations:
#         destination['regio_info'] = None
#         result = search_wikipedia(destination['regio'])
#         if not result is None:
#             destination['regio_info'] = result.summary


def search_wikipedia(search_term):
    result = None

    try:
        wikipedia.set_lang("nl")
        result = wikipedia.page(search_term)
    except:
        print(f"Kan '{search_term}' niet vinden!")

    return result

def main():
    destinations = read_destinations()
    print("""[T-Reizen]
1. Toon bestemmingen
2. Bestemming toevoegen
3. Verwijder bestemming
4. Pas bestemming aan
5. Bestemmingen opslaan
6. Zoek in wikipedia
7. Update summaries
9. Export HTML
0. Afsluiten""")

    choice = input("Maak een keuze: ")
    while choice != '0':
        if choice == '1':
            pretty_print(destinations)
        elif choice == '2':
            # Add a new destination
            destination = add_destination()
            destinations.append(destination)
        elif choice == '3':
            # Remove destination
            rem_destination(destinations)
        elif choice == '4':
            # Edit destination
            print('TODO')
        elif choice == '5':
            # Save destinations
            save_destinations(destinations)
            destinations = read_destinations()
        elif choice == '6':
            zoek_term = input("Wat wil je zoeken op Wikipedia? ")
            result = search_wikipedia(zoek_term)
            if result is not None:
                print(result.title)
        elif choice == '7':
            # Update summaries
            update_summaries(destinations)
        elif choice == '9':
            # fetch_wiki_data(destinations)
            export_html(destinations)

        #  Ask for another choice
        choice = input("Maak nog een keuze: ")

if __name__ == "__main__":
    main()