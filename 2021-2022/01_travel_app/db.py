import sqlite3
import yaml
import wikipedia
from geopy.geocoders import Nominatim

DB_FILE = "t-reizen.db"

def init_db():
    con = sqlite3.connect(DB_FILE)
    cur = con.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS country (
        id          INTEGER       PRIMARY KEY AUTOINCREMENT,
        name        VARCHAR (255),
        description VARCHAR (511),
        latitude    DOUBLE,
        longitude   DOUBLE,
        zoom_level  INTEGER
    );
    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS region (
        id              INTEGER       PRIMARY KEY AUTOINCREMENT,
        name            VARCHAR (255) NOT NULL,
        avg_temperature INTEGER,
        description     VARCHAR (511),
        category        VARCHAR (127),
        latitude        DOUBLE,
        longitude       DOUBLE,
        country_id      INTEGER       CONSTRAINT fk_country REFERENCES country (id) 
    );
    """)
    con.close()


def read_countries():
    con = sqlite3.connect(DB_FILE)
    cur = con.cursor()
    # Fetch countries
    cur.execute("SELECT * FROM country c")
    data = cur.fetchall()
    countries = []
    for data_row in data:
        countries.append({
            'id': data_row[0],
            'name': data_row[1],
            'description': data_row[2],
            'latitude': data_row[3],
            'longitude': data_row[4],
            'regions': [],
        })
    return countries


def read_countries_join():
    con = sqlite3.connect(DB_FILE)
    cur = con.cursor()
    # Fetch countries
    cur.execute("""
    SELECT 
        c.id AS country_id,
        c.name AS country_name, 
        c.description AS country_description, 
        c.latitude AS country_latitude,
        c.longitude AS country_longitude,
        r.name AS region_name,
        r.description AS region_description
    FROM country c
    JOIN region r ON r.country_id = c.id
    """)
    data = cur.fetchall()
    countries = []
    for data_row in data:
        if data_row[0] not in [c['id'] for c in countries]:
            countries.append({
                'id': data_row[0],
                'name': data_row[1],
                'description': data_row[2],
                'latitude': data_row[3],
                'longitude': data_row[4],
                'regions': [],
            })
        country = find_country(countries, data_row[0])
        region = {
            'name': data_row[5],
        }
        country['regions'].append(region)

    return countries


def find_country(countries, country_id):
    for country in countries:
        if country['id'] == country_id:
            return country
    return None

def read_regions(country_id):
    con = sqlite3.connect(DB_FILE)
    cur = con.cursor()
    # Fetch regions
    cur.execute("SELECT * FROM region WHERE country_id = ?", (country_id,))
    data = cur.fetchall()
    regions = []
    for region in data:
        regions.append({
            'id': region[0],
            'name': region[1],
            'description': region[3],
            'latitude': region[5],
            'longitude': region[6],
        })
    return regions


def save_country(country):
    con = sqlite3.connect(DB_FILE)
    cur = con.cursor()
    # Fetch country
    cur.execute("SELECT id FROM country WHERE name = :name", country)
    countries = cur.fetchall()
    if len(countries) == 0:
        cur.execute("""INSERT INTO country (
                            name,
                            description,
                            latitude,
                            longitude,
                            zoom_level
                        )
                        VALUES (
                            ?,
                            ?,
                            ?,
                            ?,
                            ?
                        )""", 
                        (
                            country.get('name', ''), 
                            country.get('description', ''),
                            country.get('latitude'),
                            country.get('longitude'),
                            country.get('zoom_level', 0)
                        )
                    )
        country_id = cur.lastrowid
    else:
        country_id = countries[0][0]
        params = (
            country.get('name', ''), 
            country.get('description', ''),
            country.get('latitude'),
            country.get('longitude'),
            country.get('zoom_level', 0),
            country_id
        )
        cur.execute("""UPDATE country 
                        SET name = ?,
                            description = ?,
                            latitude = ?,
                            longitude = ?,
                            zoom_level = ?
                        WHERE id = ?
                        """, params)

    # Save (commit) the changes
    con.commit()
    con.close()
    return country_id


def save_region(region):
    con = sqlite3.connect(DB_FILE)
    cur = con.cursor()
    # Fetch country
    cur.execute("SELECT id FROM region WHERE name = :name", region)
    regions = cur.fetchall()
    if len(regions) == 0:
        print("Region bestaat nog niet")
        cur.execute("""INSERT INTO region (
                       name,
                       description,
                       category,
                       latitude,
                       longitude,
                       country_id
                   )
                   VALUES (
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?
                )""", 
                (
                    region.get('name', ''), 
                    region.get('description', ''),
                    region.get('category', ''),
                    region.get('latitude'),
                    region.get('longitude'),
                    region.get('country_id')
                )
            )
    else:
        print("Region bestaat al.")
        region_id = regions[0][0]
        params = (
            region.get('name', ''), 
            region.get('description', ''),
            region.get('category', ''),
            region.get('latitude'),
            region.get('longitude'),
            region.get('country_id', 0),
            region_id
        )
        cur.execute("""UPDATE region
                        SET name = ?,
                            description = ?,
                            category = ?,
                            latitude = ?,
                            longitude = ?,
                            country_id = ?
                        WHERE id = ?
                        """, params)

    # Save (commit) the changes
    con.commit()
    con.close()


def read_config(filename):
    with  open(filename) as yaml_file:
        parsed_yaml_file = yaml.load(yaml_file, Loader=yaml.FullLoader)
    return parsed_yaml_file    


def search_wikipedia(search_term):
    result = None
    try:
        wikipedia.set_lang("nl")
        result = wikipedia.page(search_term)
    except:
        print(f"Kan '{search_term}' niet vinden!")
    return result


def fetch_geo_location(country):
    geolocator = Nominatim(user_agent="https://nominatim.openstreetmap.org/")
    location = geolocator.geocode(country.get('name'))
    return (location.latitude, location.longitude)


if __name__ == "__main__":
    init_db()
    conf = read_config('t-reizen.yaml')
    for destination in conf['content']['destinations']:
        country = destination.get('country')
        result = search_wikipedia(country.get('name'))
        if result is not None:
            country['description'] = result.summary
        location = fetch_geo_location(country)
        country['latitude'] = location[0]
        country['longitude'] = location[1]
        country_id = save_country(country)

        for region in country.get('regions', []):
            region['country_id'] = country_id
            result = search_wikipedia(region.get('name'))
            if result is not None:
                region['description'] = result.summary
            location = fetch_geo_location(region)
            region['latitude'] = location[0]
            region['longitude'] = location[1]
            save_region(region)
