from cgi import test
import json
from flask import Flask, request, jsonify


app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True


@app.route('/api/search', methods=['GET', 'POST'])
def find_restaurants():
    test_data = []
    with open("osm-result.json",'r') as file:
        test_data = json.load(file)
    print(test_data)
    # return "Effe wachte joh"
    return jsonify(test_data) 
