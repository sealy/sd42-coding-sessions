from crypt import methods
import json
from flask import Flask, request, jsonify
import overpy

api = overpy.Overpass()

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True


@app.route('/')
def hello():
    return """
<html>
<head>
    <script>
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        async function showPosition(position) {
            let x = document.getElementById("demo");
            x.innerHTML = "<p>Latitude: " + position.coords.latitude +
            "<br>Longitude: " + position.coords.longitude +"</p>";
            await showBoundingBox(position);
        }

        async function showBoundingBox(position) {
            const delta = 0.0001;
            let tl_lat = position.coords.latitude + delta;
            let tl_lon = position.coords.longitude - (2*delta);
            let br_lat = position.coords.latitude - delta;
            let br_lon = position.coords.longitude + (2*delta);
            let x = document.getElementById("bbox");
            x.innerHTML = "<p>Top left: (" + tl_lat + ", " + tl_lon +
            ")<br>Bottom right: (" + br_lat + ", " + br_lon + ")</p>";

            const response = await fetch('http://127.0.0.1:5000/rest', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ bbox: [[tl_lon,tl_lat],[br_lon, br_lat]]})
            })
            response.json().then(data => {
                let results = document.getElementById("rest");
                for(let r of data) {
                    results.innerHTML += "<li>"+JSON.stringify(r, null, 4)+"</li>";
                }
            });
        }

    </script>
</head>
<body onload="getLocation()">
    <h2>Welkom by Phreeter</h2>
    <div id='demo'></div>
    <div id='bbox'></div>
    <ol id='rest'></ol>
</body>
</html>
"""


@app.route('/rest', methods=['GET', 'POST'])
def find_restaurants():
    if request.method == "POST":
        json_data = json.loads(request.data)
        print(json_data['bbox'])
        tl = str(json_data['bbox'][1][1]) + "," + str(json_data['bbox'][1][0])
        br = str(json_data['bbox'][0][1]) + "," + str(json_data['bbox'][0][0])
        query = f"""
        node["amenity"="restaurant"]
        ({tl},{br});
        out;
        """
        print(query)
        result = api.query(query)
        # result = api.query("""
        # node["amenity"="college"]
        # (52.213985,6.8610551,52.2202931,6.8313516);
        # out;
        # """)
        with open("osm-result.json",'w') as file:
            file.write(str(result))

        print(f"Found: {len(result.nodes)}")
        restaurants = []
        for node in result.nodes:
            restaurant =  {}
            restaurant['lat'] = node.lat
            restaurant['lon'] = node.lon
            print("    Lat: %f, Lon: %f" % (node.lat, node.lon))
            for tag, value in node.tags.items():
                print(f"    * {tag}: {value}")
                if tag == "name":
                    restaurant[tag] = value
            restaurants.append(restaurant)
        return jsonify(restaurants)

    return "OK - GET"


@app.route('/find')
def find():
    ip_address = request.remote_addr
    print(ip_address)
    print("Searching...")
    # fetch all ways and nodes
        # (53.2987342,-6.3870259,53.4105416,-6.1148829); 
        # node["amenity"="restaurant"]
    result = api.query("""
        node["amenity"="college"]
        (52.213985,6.8610551,52.2202931,6.8313516);
        out;
        """)

        # way(50.746,7.154,50.748,7.157) ["highway"];
        # (._;>;);
        # out body;
        # """)

    # for node in result.nodes:
    #     print("    Lat: %f, Lon: %f" % (node.lat, node.lon))
    print(f"Found: {len(result.nodes)}")
    # for way in result.ways:
    #     print("Name: %s" % way.tags.get("name", "n/a"))
    #     print("  Highway: %s" % way.tags.get("highway", "n/a"))
    #     print("  Nodes:")
    for node in result.nodes:
        print("    Lat: %f, Lon: %f" % (node.lat, node.lon))
        for tag, value in node.tags.items():
            print(f"    * {tag}: {value}")
        
            
    return "Searching..."
