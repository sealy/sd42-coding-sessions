import os, sys, wave, numpy#, pymedia.audio.sound, scikits.samplerate



        # byteString = waveRead.readframes(1000) # Read at most 1000 samples from the file.
        # while len(byteString) != 0:
        #     self.handleEvent(pygame.event.poll()) # This does not wait for an event.
        #     fout.write(str(soundOutput.getLeft()) + "\n") # For troubleshooting
        #     if soundOutput.getLeft() < 0.2: # If there is less than 0.2 seconds left in the sound buffer.
        #         array = numpy.fromstring(byteString, dtype=numpy.int16)
        #         byteString = scikits.samplerate.resample(array, self.ratio, "sinc_fastest").astype(numpy.int16).tostring()
        #         soundOutput.play(byteString)
        #         byteString = waveRead.readframes(500) # Read at most 500 samples from the file.
        # waveRead.close()
        # return

def main():
    minOctave = -2.0
    maxOctave = 2.0
    ratio = 1.0
    waveRead = wave.open(os.path.join(sys.path[0], "music.wav"), 'rb')
    sampleRate = waveRead.getframerate()
    channels = waveRead.getnchannels()
    soundFormat = pymedia.audio.sound.AFMT_S16_LE
    soundOutput = pymedia.audio.sound.Output(sampleRate, channels, soundFormat)
        

if __name__ == '__main__':
    main()