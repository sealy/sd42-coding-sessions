from random import randint
from collections import deque
import sys
from blessed import Terminal

NUM_ITEMS = 5
ITEM_MARKER = "🐁"
SNAKE_MARKER = "🐍"

def print_grid(term, grid, items, snake):
    print(term.home + term.clear)#+ term.move_y(term.height // 2))
    for y in range(len(grid)):
        line = ''
        for x in range(len(grid[0])):
            line += " " #grid[y][x]
        print(term.black_on_lightblue(line))
    print_items(term, items)
    print_snake(term, snake)


def gerenate_item_position(grid):
    x = randint(0, len(grid[0])-1)
    x = x + (x % 2)
    y = randint(0, len(grid)-1)
    return (x, y)


def update_snake(snake, current_direction):
    head = snake.popleft()
    match current_direction:
        case "R":
            new_head = (head[0]+2, head[1])
        case "L":
            new_head = (head[0]-2, head[1])
        case "U":
            new_head = (head[0], head[1]-1)
        case "D":
            new_head = (head[0], head[1]+1)
    snake.appendleft(head)
    snake.appendleft(new_head)
    return snake
 

def check_item_hap(snake, items, grid):
    head = snake.popleft()
    tail = snake.pop()
    if head  in items:
        snake.append(tail)
        items.remove(head)
        new_item_pos = gerenate_item_position(grid)
        items.append(new_item_pos)  
    snake.appendleft(head)
    return items

def check_game_over(term, snake):
    new_head = snake.popleft()
    if new_head[0] < 0 or new_head[0] > term.width or new_head[1] < 0 or new_head[1] > (term.height-2):
        print("Game over")
        sys.exit() 
    snake.appendleft(new_head)


def clear_snake(term, snake):
    for snake_part in snake:
        print(term.move_y(snake_part[1]) + term.move_x(snake_part[0]) + term.black_on_lightblue(' '))

def print_snake(term, snake):
    for snake_part in snake:
        print(term.move_y(snake_part[1]) + term.move_x(snake_part[0]) + term.black_on_green(SNAKE_MARKER))

def print_items(term, items):
    for item in items:
        print(term.move_y(item[1]) + term.move_x(item[0]) + term.black_on_blue(ITEM_MARKER))


def main():
    grid = []
    items = []
    snake = deque([])
    term = Terminal()
    current_direction = "R"
    
    # Init grid
    for y in range(term.height):
        grid.append([])
        for _ in range(term.width):
            grid[y].append(' ')

    # Initialize random items
    for _ in range(NUM_ITEMS):
        new_item_pos = gerenate_item_position(grid)
        items.append(new_item_pos)

    snake.append((len(grid[0])//2,len(grid)//2))

    # Print grid
    print_grid(term, grid, items, snake)

    while True:
        # print_grid(term, grid, items, snake)
        clear_snake(term, snake)
        # Move snake
        snake = update_snake(snake, current_direction)
        items = check_item_hap(snake, items, grid)
        print_snake(term, snake)
        print_items(term, items)

        with term.cbreak(), term.hidden_cursor():
            # print(term.home + term.clear + term.move_y(term.height // 2) + term.black_on_blue("TEST"))
    
            inp = term.inkey(timeout=0.1)
            match repr(inp):
                case "KEY_ENTER":
                    return
                case "KEY_RIGHT":
                    current_direction = "R"
                case "KEY_LEFT":
                    current_direction = "L"
                case "KEY_UP":
                    current_direction = "U"
                case "KEY_DOWN":
                    current_direction = "D"
                case _:
                    # print("not matched")
                    pass
            check_game_over(term, snake)


if __name__ == "__main__":
    main()