from random import choice

ELEMENTS = ["ROCK", "PAPER", "SCISSORS"]
MAX_NUM_ROUNDS = 100

def determine_winner(player_1, player_2):
    index_player_1 = ELEMENTS.index(player_1)
    if player_2 == ELEMENTS[index_player_1 - 1]:
        return player_1
    elif player_1 == player_2:
        return None
    else:
        return player_2

def main():
    print("Prock - Paper - Pscizzorz")
    players = [{}, {}]
    for i in range(len(players)):
        players[i]["name"] = input(f"Vul je naam in speler {i+1}: ")
        players[i]["wins"]  = 0
        players[i]["draws"]  = 0

    has_winner = False
    for i in range(MAX_NUM_ROUNDS):
        while True:
            elements = []
            for _ in players:
                elements.append(choice(ELEMENTS))
            winner = determine_winner(elements[0], elements[1])
            if winner == None:
                for player in players:
                    player["draws"] += 1
                continue
            winner_index = elements.index(winner)
            players[winner_index]["wins"] += 1
            if players[winner_index]["wins"] > MAX_NUM_ROUNDS // 2:
                print(f"We have a winner! Congratulations {players[winner_index]['name']}")
                has_winner = True
            break
        if has_winner:
            break

    print(players)


if __name__ == "__main__":
    main()