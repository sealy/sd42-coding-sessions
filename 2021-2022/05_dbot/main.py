import datetime
import os
from abc import ABC, abstractmethod
from random import choice

import discord
import requests
from dotenv import load_dotenv
from youtubesearchpython import VideosSearch

load_dotenv()

MAX_MINUTES = 5
PROFILE_PICTURE = 'profile_picture'
ADMIN_ROLE = os.environ.get('ADMIN_ROLE','Teacher')
DISCORD_SECRET = os.environ.get('DISCORD_SECRET','')
print(ADMIN_ROLE)
print(DISCORD_SECRET)

class MusicRequestMessage():

    def __init__(self, message):
        self._message = message

    @property
    def message_type(self):
        if self._message.startswith("!"):
            # !mm messages
            return self._message.split(" ")[0]
        else:
            # When the crowd says bot!
            return self._message

class MusicRequestProcessor(ABC):

    @abstractmethod
    async def process(self, message):
        pass

class MusicRequestListProcessor(MusicRequestProcessor):

    async def process(self, _):
        if not MyClient.requests:
            return "No requests"
        else:
            titles = [f"{index+1}: {r['title']}" for index, r in enumerate(MyClient.requests)]
            return "\n".join(titles)


class MusicRequestAddProcessor(MusicRequestProcessor):

    def _validate(self, video):
        duration = video.get('duration', "8:00")
        list = duration.split(":")
        if len(list) > 2:
            return False
        duration_seconds = int(list[1]) + int(list[0]) * 60 
        if duration_seconds > MAX_MINUTES * 60:
            return False
        return True

    async def process(self, message):
        req = message.content[len('!mm_add '):].lstrip()
        videosSearch = VideosSearch(req, limit = 3)
        print(videosSearch.result())
        if len(videosSearch.result()) > 0:
            results = videosSearch.result()
            video = {
                "keyword": req,
                "title": results['result'][0].get('title'),
                "link": results['result'][0].get('link'),
                "duration": results['result'][0].get('duration'),
                "thumbnail": results['result'][0].get('thumbnails')[0],
            }
            if self._validate(video):
                MyClient.requests.append(video)
                return f'Great success! - {video.get("title")}: {video.get("link")}'
            else:
                return f'Great success! - NOT'
        else:
            return 'No success!'


class MusicRequestProtectedProcessor(MusicRequestProcessor):

    @abstractmethod
    async def protected_process(self, message):
        pass


    async def process(self, message):
        for role in message.author.roles:
            if role.name == ADMIN_ROLE:
                result = await self.protected_process(message)
                return result
        else:
            return "Computer says no!" 



class MusicRequestInitProcessor(MusicRequestProtectedProcessor):
    
    def _get_current_thread(self, threads):
        name = datetime.datetime.now().strftime('%Y%m%d')
        for t in threads:
            if t.name == name:
                return t
        return None

    async def protected_process(self, message):
        thread = self._get_current_thread(message.channel.threads)
        if thread is None:
            await message.channel.create_thread(
                name=datetime.datetime.now().strftime('%Y%m%d'), 
                message=message, 
                auto_archive_duration=60
            )
        else:
            return "Computer says no!"


class MusicRequestRejectProcessor(MusicRequestProtectedProcessor):

    async def protected_process(self, message):
        req = message.content[len('!mm_reject '):].lstrip()
        if req.isdigit():
            del MyClient.requests[int(req)-1]
            return "And its gone..."


class MusicRequestSelectProcessor(MusicRequestProtectedProcessor):

    def download_image(self, url):
        r = requests.get(url)
        with open(PROFILE_PICTURE, 'wb') as f:
            f.write(r.content) 

    async def protected_process(self, message):
        selected = choice(MyClient.requests)
        self.download_image(selected.get("thumbnail").get("url"))
        fp = open(PROFILE_PICTURE, 'rb')
        # await client.user.edit(avatar=fp.read())
        return f"SELECTA: {selected.get('title')} - {selected.get('link')}"

class MyClient(discord.Client):
    _mapping = {
        "!mm": MusicRequestListProcessor(),
        "!mm_add": MusicRequestAddProcessor(),
        "!mm_init": MusicRequestInitProcessor(),
        "!mm_reject": MusicRequestRejectProcessor(),
        "When the crowd says bot!": MusicRequestSelectProcessor()
    } 
    requests = []

    def __init__(self):
        super(). __init__(intents=discord.Intents.all())

    async def on_ready(self):
        print('Logged on as', self.user)


    async def on_message(self, message):
        # don't respond to ourselves
        if message.author == self.user:
            return

        print(f"RECEIVED: {message}")
        print(f"RECEIVED: {message.author}")
        print(f"RECEIVED: {message.content}")

        # Random stuff
        msg = message.content
        if msg.lower() == 'ching':
            await message.channel.send('chong')

        if msg.lower() == 'chong':
            for role in message.author.roles:
                if role.name == 'Teacher':
                    await message.channel.send('schwing')
                    break
            else:
                await message.channel.send('ching')

        # Actual message processing.
        music_req = MusicRequestMessage(msg)
        if music_req.message_type in self._mapping:
            result = await self._mapping[music_req.message_type].process(message)
            if result:
                await message.channel.send(result)
            return



client = MyClient()
client.run(DISCORD_SECRET)
