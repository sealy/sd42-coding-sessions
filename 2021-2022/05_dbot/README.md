## Morning Music bot

In this project we have build a Discord bot that allows users to send song requests.
The bot searches Youtube for the request and selects a random request from the list.

Due to the huge success the code has been moved to a separate repository: https://gitlab.com/sealy/mm_bot