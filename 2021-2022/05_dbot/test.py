from youtubesearchpython import VideosSearch
from pytube import YouTube
from pydub import AudioSegment


def test():
    vid_file = "Rick Astley - Never Gonna Give You Up (Official Music Video)-dQw4w9WgXcQ.mp3"
    wav = AudioSegment.from_file(vid_file)
    getaudio = wav.export("test.wav", format="wav")    

def main():
    # YouTube("https://www.youtube.com/watch?v=VZAJMBFq85s").streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first().download()
    req = input("Keyword voor youtube: ")
    videosSearch = VideosSearch(req, limit = 3)
    # print(videosSearch.result())
    results = videosSearch.result()
    # print(results)
    print(f"Duration: {results['result'][0].get('duration')}")
    print(f"Link: {results['result'][0].get('link')}")
    print(f"Thumbnails: {results['result'][0].get('thumbnails')}")
    if len(videosSearch.result()) > 0:
        print("Results")

if __name__ == "__main__":
    test()