import youtube_dl
from pydub import AudioSegment


class MyLogger(object):
    def debug(self, msg):
        print(f"[DEBUG] {msg}")

    def warning(self, msg):
        print(f"[WARN] {msg}")

    def error(self, msg):
        print(f"[ERROR] {msg}")


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
}
with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    ydl.download(['https://www.youtube.com/watch?v=dQw4w9WgXcQ'])

vid_file = "Rick Astley - Never Gonna Give You Up (Official Music Video)-dQw4w9WgXcQ.mp3"
wav = AudioSegment.from_file(vid_file)
getaudio = wav.export("test.wav", format="wav")

# import pafy
# import vlc

# url = ""
# video = pafy.new(url)
# best = video.getbest()
# playurl = best.url
# print(playurl)

# Instance = vlc.Instance()
# player = Instance.media_player_new()
# Media = Instance.media_new(playurl)
# Media.get_mrl()
# player.set_media(Media)
# player.play()
  
# # creating vlc media player object
# media = vlc.MediaPlayer(best.url)
  
# # start playing video
# media.play()