from logging import error
from flask import Flask, request, render_template, redirect
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///iglogram.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    password = db.Column(db.String(100))

    def __init__(self, name, password):
       self.name = name
       self.password = password

db.create_all()

@app.route("/", methods = ['POST','GET'])
def home_page():
    if request.method == 'POST':
        print("We zijn aan het POSTen")
        if request.form.get("register") != None:
            username = request.form.get("username")
            results = User.query.filter_by(name = username).all()
            if len(results) == 0:
                user = User(username, request.form.get("password"))
                db.session.add(user)
                db.session.commit()
            else:
                print("ERROR: Gebruiker bestaat al!")
        else:
            print(request.form.get("login"))
    return render_template("home.html")    


@app.route("/profile")
def profile_page():
    photos = ["default" for _ in range(9)]
    print(photos)
    return render_template("profile.html", photos=photos)


@app.route("/test")
def test_page():
    return render_template("index.html", title="Test")


@app.route("/upload", methods = ['POST','GET'])
def upload_page():
    if request.method == 'POST':
        print("We zijn aan het POSTen")
        file = request.files['file_name']
        if file:
            DEFAULT_FILE_NAME = "default"
            file_path = app.static_folder + "/images/" + DEFAULT_FILE_NAME
            file.save(file_path)
            return redirect("/")
        else:
            errors = ["U heeft geen file gekozen"]
            return render_template("upload.html", title="Upload", errors=errors) 
    else:
        print("We zijn aan het GETen")
    return render_template("upload.html", title="Test")
