from abc import ABC, abstractmethod, abstractproperty
from asyncio import protocols
from email.utils import encode_rfc2231
import enum
from secrets import choice
import pygame

BLOCK_SIZE = 40
WIDTH = BLOCK_SIZE * 10
HEIGHT = BLOCK_SIZE * 19
BACKGROUND_COLOR = (255,255,255)
BORDER_COLOR = (128, 128, 128)
DEFAULT_COLOR = (0,0,0)
FRAME_RATE = 30

BLUE_COLOR = (0, 0, 255)
YELLOW_COLOR = (255, 255, 0)
ORANGE_COLOR = (255, 127, 0)

class Shape(ABC):

    def __init__(self, color=DEFAULT_COLOR):
        self.color = color
        self.x = (WIDTH // 2) - BLOCK_SIZE
        self.y = - BLOCK_SIZE
        self._pixels = []
        
    def draw(self, screen):
        for pixel in self.pixels:
            pixel.draw(screen)

    def move(self, field, direction = None):
        # TODO: Forecasting collisions
        if direction == "LEFT":
            self._move_left()
            if not self.within_bounds() or self.has_collided(field):
                self._move_right()
        elif direction == "RIGHT":
            self._move_right()
            if not self.within_bounds() or self.has_collided(field):
                self._move_left()
        elif direction == "DOWN":
            self._drop(field)
        elif direction == "UP":
            self.rotate()
        else:
            self._drop(field)

    def _move_left(self):
        for pixel in self.pixels:
            pixel.x -= BLOCK_SIZE

    def _move_right(self):
        for pixel in self.pixels:
            pixel.x += BLOCK_SIZE

    def _drop(self, field):
        if not self.has_collided(field):
            for pixel in self.pixels:
                pixel.y += BLOCK_SIZE                

    def within_bounds(self):
        for pixel in self.pixels:
            if pixel.y + BLOCK_SIZE > HEIGHT or \
                pixel.x < 0 or \
                pixel.x + BLOCK_SIZE > WIDTH:
                return False
        return True

    def has_collided(self, field):
        for my_pixel in self.pixels:
            if my_pixel.y + BLOCK_SIZE >= HEIGHT:
                return True
            for pixel in field:
                if my_pixel.y + BLOCK_SIZE == pixel.y and my_pixel.x == pixel.x:
                    return True
        return False

    @property
    def pixels(self):
        return self._pixels

    @abstractmethod
    def rotate(self):
        pass


class Pixel():

    def __init__(self, x=0, y=0, color=DEFAULT_COLOR):
        self.color = color
        self.x = x
        self.y = y

    def draw(self, screen):
        pygame.draw.rect(screen, self.color, (self.x, self.y, BLOCK_SIZE, BLOCK_SIZE))
    
    def __str__(self):
        return f"Pixel at ({self.x},{self.y})"


class SmashBoy(Shape):

    def __init__(self, color=YELLOW_COLOR):
        super().__init__(color)
        self._pixels = [
            Pixel(self.x, self.y, self.color),
            Pixel(self.x + BLOCK_SIZE, self.y, self.color),
            Pixel(self.x, self.y + BLOCK_SIZE, self.color),
            Pixel(self.x + BLOCK_SIZE, self.y + BLOCK_SIZE, self.color),
        ]

    def rotate(self):
        # A smashboy stays the same after rotation.
        pass


class OrangeRicky(Shape):

    __ROTATIONS = [
        [(-BLOCK_SIZE,BLOCK_SIZE), (0,BLOCK_SIZE), (BLOCK_SIZE, BLOCK_SIZE), (BLOCK_SIZE, 0)],
        [(BLOCK_SIZE,-BLOCK_SIZE), (0,-BLOCK_SIZE), (-BLOCK_SIZE, -BLOCK_SIZE), (-BLOCK_SIZE, 0)],
        [(-BLOCK_SIZE,-BLOCK_SIZE), (-BLOCK_SIZE, 0), (-BLOCK_SIZE, BLOCK_SIZE), (0, BLOCK_SIZE)],
        [(BLOCK_SIZE,BLOCK_SIZE), (BLOCK_SIZE,0), (BLOCK_SIZE, -BLOCK_SIZE), (0,-BLOCK_SIZE)]
    ]
    
    def __init__(self, color=ORANGE_COLOR):
        super().__init__(color)
        self._current_rotation = 0
        self._should_rotate = True
        # self.y += 3 * BLOCK_SIZE
        self._pixels = [Pixel(self.x, self.y, self.color) for _ in range(4)]
    
    @property
    def pixels(self):
        if self._should_rotate:
            for index, rotation in enumerate(self.__ROTATIONS[self._current_rotation]):
                self._pixels[index].x += rotation[0]
                self._pixels[index].y += rotation[1]
            self._should_rotate = False
        return self._pixels
        # return [Pixel(self.x + x_offset, self.y + y_offset, self.color) for (x_offset, y_offset) in OrangeRicky.__ROTATIONS[self._current_rotation]]
        # TODO
    

    def rotate(self):
        self._current_rotation = (self._current_rotation + 1) % len(OrangeRicky.__ROTATIONS)
        self._should_rotate = True


def draw_shapes(screen, shapes):
    for shape in shapes:
        shape.draw(screen)

def move_shape(shapes, direction = None):
    for shape in shapes:
        if direction == "LEFT":
            if shape.x > 0:
                shape.x -= BLOCK_SIZE
        elif direction == "RIGHT":
            if shape.x + shape.width < WIDTH:
                shape.x += BLOCK_SIZE
        else:
            if shape.y + shape.height < HEIGHT:
                shape.y += BLOCK_SIZE


def draw_grid(screen):
    for w in range(0, WIDTH, BLOCK_SIZE):
        for h in range(0, HEIGHT, BLOCK_SIZE):
            pygame.draw.rect(screen, BORDER_COLOR, (w, h, BLOCK_SIZE, BLOCK_SIZE), 1)

def render():
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption('Tetris')
    screen.fill(BACKGROUND_COLOR)

    possible_shape = [SmashBoy, OrangeRicky]
    current_shape = choice(possible_shape)()
    field = []

    frame_counter = 0
    playing = True
    while playing:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                playing = False
                break
            if event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                current_shape.move(field, "LEFT")
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                current_shape.move(field, "RIGHT")
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
                current_shape.move(field, "DOWN")
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
                current_shape.move(field, "UP")

        screen.fill(BACKGROUND_COLOR)
        draw_grid(screen)
        current_shape.draw(screen)
        for pixel in field:
            pixel.draw(screen)
        if frame_counter == 0:
            current_shape.move(field)
            if current_shape.has_collided(field):
                field.extend(current_shape.pixels)
                current_shape = choice(possible_shape)()
            # TODO: Get all rows of pixels in the field and count them.
            # If we have 10 pixesl in a row then we should delete the row (and update the field)
            check_field_clearance(field)
            
        frame_counter = (frame_counter + 1) % FRAME_RATE

        pygame.display.flip()
