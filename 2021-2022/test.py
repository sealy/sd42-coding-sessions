import enum
import random

def weighted_choice(weights):
    totals = []
    running_total = 0

    for w in weights:
        running_total += w
        totals.append(running_total)

    rnd = random.random() * running_total
    for i, total in enumerate(totals):
        if rnd < total:
            return i
    print("None")

def main():
    users = ["Alice", "Bob", "Charlie"]
    weights = [0, 0, 0]
    ALPHA = 0.5
    MAX = len(users)

    for _ in range(1000):
        index = weighted_choice([MAX - w for w in weights])
        weights[index] = (1 / MAX) 
        for i, w in enumerate(weights): 
            weights[i] += (1 - ALPHA) * w
        print(f"{users[index]} selected")
        print(weights)
        

 
# def main():
#     lst = [24, 0]
#     keuze = int(input("kies een getal: "))
#     index_keuze = -1 
#     for index, value in enumerate(lst):
#         if value == keuze:
#             print(f"Getal zit op index {index}")
#             index_keuze  = index
#             break
#     index_keuze = lst.index(keuze)
#     lst[index_keuze] = lst[index_keuze] + 3
#     print(lst) 

from audioplayer import AudioPlayer

# Playback stops when the object is destroyed (GC'ed), so save a reference to the object for non-blocking playback.
AudioPlayer("path/to/somemusic.mp3").play(block=True)
    

if __name__ == "__main__":
    main()