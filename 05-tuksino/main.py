
from abc import ABC, abstractmethod
import random

class User:
    def __init__(self, id):
        self._id = id
        self._wallet = Wallet()
        self._coins = 0

    def get_coins(self):
        return self._coins

    def add_coins(self, coins):
        self._coins += coins
    
    def decrement_credits(self, credits):
        self._wallet.remove(credits)
    
    def bet(self, amount):
        result = False
        if amount > self._coins:
            print("Not enough coins")
        else:
            self._coins -= amount
            result = True
        return result

    def __str__(self) -> str:
        return f"User ({self._id}) current credits {self._wallet}, current coins {self._coins}"


class Wallet:
    def __init__(self):
        self._credits = 0

    def remove(self, amount):
        self._credits -= amount

    def __str__(self):
        return f"[{self._credits}]"


class Game(ABC):

    def __init__(self, user_coins, shop_coins):
        self.earnings = user_coins + shop_coins

    @abstractmethod
    def play(self):
        pass

class HouseAlwaysWins(Game):

    def play(self):
        return False


class Baccarat(Game):
    
    # TODO: Implement game logic

    def play(self):
        return True


class SimShop:

    def __init__(self):
        self._piggy_bank = 0

    def bet(self, amount):
        result = amount
        if self._piggy_bank < amount:
            result = self._piggy_bank
        self._piggy_bank -= result
        return result

    def run(self):
        users = [User(i) for i in range(10)]
        while True:
            user = random.choice(users)
            will_gamble = random.randint(0,10)
            if will_gamble == 0:
                # Let's get gambling
                amount_coins = user.get_coins() 
                if amount_coins > 0:
                    user_amount = random.randint(0, amount_coins)
                    user.bet(user_amount)
                    shop_amount = self.bet(user_amount) 
                    print(f"Gambling: user amount {user_amount} - shop amount {shop_amount}")
                    game = HouseAlwaysWins(user_amount, shop_amount)
                    if game.play():
                        print("User wins!")     
                        user.add_coins(game.earnings)
                        print(user)  
                    else:
                        print("The house wins!")     
                        self._piggy_bank += game.earnings
            else:
                # Buy stuff
                credits = random.randint(50,100)
                user.decrement_credits(credits)
                print(f"{user} bought for {credits} eurocents snacks")
                coins = self.buy(credits)
                user.add_coins(coins)
            
            print(f"Piggy bank is now: {self._piggy_bank}")
            input("< Press enter >")

    def buy(self, credits):
        self._piggy_bank += credits // 10
        return credits


if __name__ == "__main__":
    SimShop().run()