from pydub import AudioSegment
from pydub.playback import play
import sounddevice as sd
from scipy.io.wavfile import write

# First record something
filename = 'output.wav'
input("Press <Enter> to start a 5 second recording.")
fs = 44100  # Sample rate
seconds = 5  # Duration of recording

myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=1)
sd.wait()  # Wait until recording is finished
write(filename, fs, myrecording)  # Save as WAV file 

print(f"Recording has been write to {filename}")

# Load into PyDub
loop = AudioSegment.from_wav("guitar.wav")
rec =  AudioSegment.from_wav(filename)

# Play the result
play(loop.overlay(rec.speedup().low_pass_filter(cutoff=1200).fade_in(1000), position=1000))
