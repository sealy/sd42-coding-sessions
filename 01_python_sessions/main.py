import random

MIN = 8
MAX = 12
EXTRA_ATTEMPTS = random.randint(0,5)

number_of_attempts = random.randint(MIN,MAX)
secret_number = random.randint(0,1000)

print("Welcome to our higher lower game")
guess_number_of_attempts = input(f"How many attempts do you thing you have? Should be some number between {MIN} and {MAX}: ")
if guess_number_of_attempts == str(number_of_attempts):
    number_of_attempts = number_of_attempts + EXTRA_ATTEMPTS
    print(f"BONUS! ({EXTRA_ATTEMPTS})")
print(f"You have {number_of_attempts} attempts!")

while True:
    guess = input("Please fill in your guess (it should be a number): ")
    number_of_attempts = number_of_attempts - 1 
    if guess.isdigit():
        guess = int(guess)
        if guess == secret_number:
            print("Jeej!")
            break
        elif guess > secret_number:
            print("Your guess was higher than our secret number")
        else:
            print("Your guess was lower than our secret number")
    else:
        print("Your guess is not number you moron")
    
    print(f"You have {number_of_attempts} left")
    if number_of_attempts == 0:
        print(f"Game over. The number we are looking for is {secret_number}")
        break
