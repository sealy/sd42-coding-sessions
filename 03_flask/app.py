from crypt import methods
from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///db.sqlite3"
db = SQLAlchemy(app)


class LoempiaKraam(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String)
    longitude = Column(String)
    latitude = Column(String)

db.create_all()


@app.route('/')
def hello_world():
    return render_template("index.html")

@app.route('/loempias', methods=["GET"])
def new_loempia():
    return render_template("new_loempia.html")


@app.route('/loempias/<id>', methods=["GET"])
def get_loempia(id):
    print(f"{request.method}_LOEMPIA: {id}")
    return render_template("new_loempia.html")


@app.route('/loempias', methods=["POST"])
def post_loempia():
    print("POST_LOEMPIA")
    return render_template("new_loempia.html")


# @app.route('/loempias/<id>', methods=["PUT"])
# def put_loempia(id):
#     print("PUT_LOEMPIA")
#     return render_template("new_loempia.html")


# @app.route('/loempias/<id>', methods=["DELETE"])
# def delete_loempia(id):
#     print("PUT_LOEMPIA")
#     return render_template("new_loempia.html")



if __name__ == '__main__':
   app.run()